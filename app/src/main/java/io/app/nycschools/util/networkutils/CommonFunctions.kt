package io.app.nycschools.util.networkutils

import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import retrofit2.HttpException


object CommonFunctions {

    fun networkRegistration(): IntentFilter {
        var intentFilter = IntentFilter()
        intentFilter.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        intentFilter.priority = 100
        return intentFilter
    }


    fun isOnline(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        // For 23 api or above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ->    true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) ->   true
                capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ->   true
                else ->     false
            }
        }
        // For below 23 api
        else {
            if (connectivityManager.activeNetworkInfo != null && connectivityManager.activeNetworkInfo!!.isConnected) {
                return true
            }
        }
        return false
    }

    fun getErrorData(
        it: Throwable
    ): String? {
        var errorData: String? = null
        if (it is HttpException) {
            if (it.code() != 200) {
                errorData =
                    it.response()?.errorBody()?.string()
            }
        }
        return errorData
    }



//    fun getAddress(context: Context, latLng: LatLng): String {
//        val gcd = Geocoder(context, Locale.getDefault())
//        var addresses: List<Address>? = null
//        var applocation: String = ""
//        try {
//            addresses = gcd.getFromLocation(
//                latLng.latitude,
//                latLng.longitude,
//                1
//            )
//            if (addresses.size > 0) {
//                applocation = addresses[0].getAddressLine(0)
//
//
//            }
//        } catch (e: Exception) {
//            e.printStackTrace()
//        }
//        return applocation
//    }
}