package io.app.nycschools.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.SystemClock
import android.view.View
import android.widget.Toast


object RideUtils {

    fun makeAPhoneCall(context: Context, phoneNumber: String) {
        try {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:$phoneNumber")
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(context, "Unable to make a call", Toast.LENGTH_SHORT).show()
        }
    }

    fun sendMessage(context: Context, phoneNumber: String) {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("sms:$phoneNumber")
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(context, "Unable to send sms", Toast.LENGTH_SHORT).show()
        }
    }

    fun shareRideInfo(context: Context, phoneNumber: String) {
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("sms:$phoneNumber")
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
            Toast.makeText(context, "Unable to send sms", Toast.LENGTH_SHORT).show()
        }
    }

    fun View.clickWithDebounce(debounceTime: Long = 1500L, action: () -> Unit) {
        this.setOnClickListener(object : View.OnClickListener {
            private var lastClickTime: Long = 0
            override fun onClick(v: View) {
                if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
                else action()
                lastClickTime = SystemClock.elapsedRealtime()
            }
        })
    }


//    fun getVehicleIcon(vehicleType: String): BitmapDescriptor {
//        return when (vehicleType) {
//            "2" -> {
//                BitmapDescriptorFactory.fromResource(R.drawable.bikemaps)
//            }
//            "3" -> {
//                BitmapDescriptorFactory.fromResource(R.drawable.autofinal)
//            }
//            else -> {
//                BitmapDescriptorFactory.fromResource(R.drawable.ic_car_marker)
//            }
//        }
//    }

}