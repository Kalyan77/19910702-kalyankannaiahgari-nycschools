package io.app.nycschools.util

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context.log(string: String) {
    Log.v("TAG", string)
}

fun String.isValidEmail() =
    this.isNotEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()

//fun showAlertDialog(context: Context?,
//                       title: String?,
//                       message: String?) {
//    val mDialog = Dialog(context!!)
//    mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
//    mDialog.setContentView(R.layout.custom_dialog)
//    mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//    mDialog.setCanceledOnTouchOutside(false)
//    mDialog.setCancelable(false)
//
//    val txtAlertTitle: TextView = mDialog.findViewById(R.id.txtAlertTitle) as TextView
//    val txtAlertForCancel: TextView = mDialog.findViewById(R.id.txtAlertForCancel) as TextView
//    val okAlertBtn: Button = mDialog.findViewById(R.id.okAlertBtn) as Button
//
//    txtAlertTitle.text = title
//    txtAlertForCancel.text = "Your booking with $message has been cancelled."
//
//    okAlertBtn.setOnClickListener {
//        mDialog.dismiss()
//    }
//
//    mDialog.show()
//}


fun String.isValidIFC() =
    Pattern.compile(
        "^[A-Z]{4}[0-9]{3}[A-Z0-9]{1}[0-9]{3}$"
    ).matcher(this).matches()


fun String.isEmailValid() =
    Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    ).matcher(this).matches()


inline fun View.showSnackBar(message: String) {
    val snack = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    snack.show()
}

fun String.isVehicleNumberValid() =
    Pattern.compile(
        "^[a-zA-Z]{2}[0-9]{2}[a-zA-Z]{2}[0-9]{4}\$"
    ).matcher(this).matches()

//"^[A-Z|a-z]{2}[9]{2}[A-Z|a-z]{2}[0-9]{4}\$"
fun String.isValidMobileNumber() =
    Pattern.compile(
        "^[6-9]\\d{9}\$"
    ).matcher(this).matches()

fun String.isValidPanNumber() =
    Pattern.compile(
        "[A-Z]{5}[0-9]{4}[A-Z]{1}"
    ).matcher(this).matches()


fun String.isValidAdhaarNumber() =
    Pattern.compile(
        "^[2-9]{1}[0-9]{3}\\\\s[0-9]{4}\\\\s[0-9]{4}\$"
    ).matcher(this).matches()


fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}

fun Activity.hideKeyboard() {
    hideKeyboard(currentFocus ?: View(this))
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}