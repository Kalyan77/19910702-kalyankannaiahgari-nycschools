package io.app.nycschools.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.SystemClock
import android.util.Log
import androidx.annotation.RequiresApi
import java.lang.ref.WeakReference
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class Utils {
    companion object {
        // Network Check
        @RequiresApi(Build.VERSION_CODES.N)
        fun isNetworkAvailable(context: Context): Boolean {
            var result: Boolean = false
            val weakReference = WeakReference<Context>(context)

            val connectivityManager = weakReference.get()
                ?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val activeNetwork =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {

                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                activeNetwork.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }


            return result
        }

//        fun getBounds(location: LatLng, mDistanceInMeters: Int): LatLngBounds {
//            val latRadian = Math.toRadians(location.latitude)
//            val degLatKm = 110.574235
//            val degLongKm = 110.572833 * cos(latRadian)
//            val deltaLat = mDistanceInMeters / 1000.0 / degLatKm
//            val deltaLong = mDistanceInMeters / 1000.0 / degLongKm
//            val minLat: Double = location.latitude - deltaLat
//            val minLong: Double = location.longitude - deltaLong
//            val maxLat: Double = location.latitude + deltaLat
//            val maxLong: Double = location.longitude + deltaLong
//            return LatLngBounds(LatLng(minLat, minLong), LatLng(maxLat, maxLong))
//        }

        fun logInfo(tag: String, value: String) {
            Log.d(tag, value)
        }
        private var lastClickTime: Long = 0

        fun isFastDoubleClick(): Boolean {
            val time = System.currentTimeMillis()
            val timeD: Long = time - lastClickTime
            if (0 < timeD && timeD < 1500) {
                return true
            }
            lastClickTime = time
            return false
        }

        var mLastClickTime=0L
        fun isOpenRecently():Boolean{
            if (SystemClock.elapsedRealtime() - mLastClickTime < 3000){
                return true
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            return false
        }


        /**
         * This method is used to capitalize first Letter
         */
        fun capitalizeFirstLetter(s: String): String? {
            return if (s.length == 0) s else s.substring(0, 1).toUpperCase() + s.substring(1)
                .toLowerCase()
        }

//        fun BitmapDescriptorFromVector(context: Context,id: Int): BitmapDescriptor? {
//            val vectorDrawable = ContextCompat.getDrawable(context, id)
//            val bitmap = Bitmap.createBitmap(
//                vectorDrawable!!.intrinsicWidth,
//                vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
//            )
//            val canvas = Canvas(bitmap)
//            vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
//            //DrawableCompat.setTint(vectorDrawable!!, color)
//            vectorDrawable.draw(canvas)
//            return BitmapDescriptorFactory.fromBitmap(bitmap)
//        }


        fun convertToCurrentTimeZone(Date: String?): String? {
            var converted_date = ""
            try {
                val utcFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"))
                val date: Date = utcFormat.parse(Date)
                val currentTFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd hh:mm aa")
                currentTFormat.setTimeZone(TimeZone.getTimeZone(getCurrentTimeZone()))
                converted_date = currentTFormat.format(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return converted_date
        }

        fun getCurrentTimeZone(): String? {
            val tz = Calendar.getInstance().timeZone
            return tz.id
        }


    }


}