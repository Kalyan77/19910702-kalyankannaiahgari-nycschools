package io.app.nycschools.util

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.edit

//with(PreferenceManager) {
//    lattitude = location.latitude.toString()
//    longitude = location.longitude.toString()
//}
//lattitude = PreferenceManager.lattitude

object PreferenceManagerNew {
    private lateinit var sharedPreferences: SharedPreferences

    private const val GroWero_PREFERENCES = "Nyc Prefs"
    private const val IS_FIRST_TIME_LOGIN = "firstTimeLogin"
    private const val PROFILE_MOBILE_NUMBER = "profileMobileNumber"

    private val FIRST_LAUNCH = "firstLaunch"
    private const val SECURE_PIN = "securePin"

    private const val EMP_ID = "EMP_ID"
    private const val MOBILENUM = "mobile"
    private const val LOGGED_NAME = "LOGGED"
    private const val IS_LOGIN = "Islogin"
    private const val LOGGED_AS = "LOGGED_AS"

    private const val DEVICE_FCM_TOKEN = "DEVICE_FCM_TOKEN"

    internal lateinit var spEditor: SharedPreferences.Editor

    private val CLEAR_PROFILE_PREFS_KEYS = listOf(SECURE_PIN)
    private val UNACCEPTABLE_PREFS_NULL_TYPES = listOf(Boolean::class, Int::class, Float::class, Long::class)

    fun init(context: Context) {
        if (!::sharedPreferences.isInitialized) {
            sharedPreferences = context.getSharedPreferences(
                GroWero_PREFERENCES,
                AppCompatActivity.MODE_PRIVATE
            )
        }
    }

    fun setFirstTimeLaunch(isFirstTime: Boolean) {
        spEditor.putBoolean(FIRST_LAUNCH, isFirstTime)
        spEditor.commit()
    }

    fun FirstLaunch(): Boolean {
        return sharedPreferences.getBoolean(FIRST_LAUNCH, true)
    }

    var mobile: String
        get() = getFromPrefs(MOBILENUM, "")
        set(value) = setToPrefs(MOBILENUM, value)

    var deviceToken: String
        get() = getFromPrefs(DEVICE_FCM_TOKEN, "")
        set(value) = setToPrefs(DEVICE_FCM_TOKEN, value)



    var employeeID: String
        get() = getFromPrefs(EMP_ID, "")
        set(value) = setToPrefs(EMP_ID, value)

    var logged_name: String
        get() = getFromPrefs(LOGGED_NAME, "")
        set(value) = setToPrefs(LOGGED_NAME, value)

    var loggedInAs: String
        get() = getFromPrefs(LOGGED_AS, "")
        set(value) = setToPrefs(LOGGED_AS, value)

    var islogin: Boolean
        get() = getFromPrefs(IS_LOGIN, false)
        set(value) = setToPrefs(IS_LOGIN, value)

    var isEmployeeLogged: Boolean
        get() = getFromPrefs("isEmployeeLogged", false)
        set(value) = setToPrefs("isEmployeeLogged", value)

    var isSalePersonLogged: Boolean
        get() = getFromPrefs("isSalePersonLogged", false)
        set(value) = setToPrefs("isSalePersonLogged", value)

    var isFirstTimeLogin: Boolean
        get() = getFromPrefs(IS_FIRST_TIME_LOGIN, false)
        set(value) = setToPrefs(IS_FIRST_TIME_LOGIN, value)

    var profileMobileNumber: String?
        get() = getFromPrefs(PROFILE_MOBILE_NUMBER, null)
        set(value) = setToPrefs(PROFILE_MOBILE_NUMBER, value)


    var securePin: String
        get() = getFromPrefs(SECURE_PIN, "")
        set(value) = setToPrefs(SECURE_PIN, value)


    fun clearAllPreferences() {
        checkHasSharedPrefsInitialized()
        sharedPreferences.edit { clear() }
    }

    fun clearProfilePreferences() {
        checkHasSharedPrefsInitialized()
        sharedPreferences.edit {
            CLEAR_PROFILE_PREFS_KEYS.forEach { remove(it) }
        }
    }

    @Suppress("IMPLICIT_CAST_TO_ANY", "UNCHECKED_CAST")
    private inline fun <reified T> getFromPrefs(key: String, default: T): T {
        checkHasSharedPrefsInitialized()
        if (default == null && UNACCEPTABLE_PREFS_NULL_TYPES.any { T::class == it }) {
            throw IllegalArgumentException("$default of type ${T::class.simpleName} cannot be null ")
        }

        return when (T::class) {
            Boolean::class -> sharedPreferences.getBoolean(key, default as Boolean)
            String::class -> sharedPreferences.getString(key, default as? String)
            Int::class -> sharedPreferences.getInt(key, default as Int)
            Float::class -> sharedPreferences.getFloat(key, default as Float)
            Long::class -> sharedPreferences.getLong(key, default as Long)
            Set::class -> sharedPreferences.getStringSet(key, default as? Set<String>)
            else -> throw IllegalArgumentException("cannot get value for $key from SharedPreferences")
        }.let { it as T }
    }

    @Suppress("UNCHECKED_CAST")
    private inline fun <reified T> setToPrefs(key: String, value: T?) {
        checkHasSharedPrefsInitialized()
        if (value == null && UNACCEPTABLE_PREFS_NULL_TYPES.any { T::class == it }) {
            throw IllegalArgumentException("$value of type ${T::class.simpleName} cannot be null ")
        }

        sharedPreferences.edit {
            when (T::class) {
                Boolean::class -> putBoolean(key, value as Boolean)
                String::class -> putString(key, value as? String)
                Int::class -> putInt(key, value as Int)
                Float::class -> putFloat(key, value as Float)
                Long::class -> putLong(key, value as Long)
                Set::class -> putStringSet(key, value as? Set<String>)
                else -> throw IllegalArgumentException("cannot set $value to $key in SharedPreferences")
            }
        }
    }

    private fun checkHasSharedPrefsInitialized() {
        if (!::sharedPreferences.isInitialized) {
            throw UninitializedPropertyAccessException("missed to call Pref.init(context). This is necessary and should be called in Application class")
        }
    }
}
