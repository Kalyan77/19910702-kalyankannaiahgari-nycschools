package io.app.nycschools.util

import java.text.SimpleDateFormat
import java.util.*

object CalenderUtils {
    const val MIN_DATE = "1940-01-01"
    //const val MAX_DATE = "2015-12-31"
    fun getFormatDate(date: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val myDate = dateFormat.parse(date)!!
        return SimpleDateFormat("dd-MM-yyyy ", Locale.US).format(myDate)
    }

    fun getFormatDateNew(date: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val myDate = dateFormat.parse(date)!!
        return SimpleDateFormat("dd-MM-yyyy hh:mm ", Locale.US).format(myDate)
    }
    fun getFormatDateNew1(date: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val myDate = dateFormat.parse(date)!!
        return SimpleDateFormat("dd-MM-yyyy hh:mm aa", Locale.US).format(myDate)
    }
//    THU 23 JULY , 10:34 AM

    fun getSingleRideDate(date: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val myDate = dateFormat.parse(date)!!
        return SimpleDateFormat("dd MMMM yyyy , hh:mm aa", Locale.US).format(myDate)
    }
    fun getFormatDateUser(date: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val myDate = dateFormat.parse(date)!!
        return SimpleDateFormat("dd-MM-yyyy", Locale.US).format(myDate)
    }

    fun getRideDate(date: String): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        val myDate = dateFormat.parse(date)!!
        return SimpleDateFormat("dd-MMM-yyyy hh:mm aa", Locale.US).format(myDate)
    }

    fun getDateInLongFormat(date: String): Long {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        val minDate = dateFormat.parse(date)
        return minDate.time
    }
}