package io.app.nycschools.util

import android.content.Context
import android.content.SharedPreferences

class PreferenceManager constructor() {

    constructor(context: Context):this(){
        sharedPreferences = context.getSharedPreferences("RIDE_PLUS", Context.MODE_PRIVATE)
        editor = sharedPreferences.edit()
    }

    companion object {
        var ACCESS_TOKEN = "ACCESS_TOKEN"
        var fragment = "fragment"
        const val DEVICE_FCM_TOKEN = "DEVICE_FCM_TOKEN"
        var Secondprogile = "Secondprogile"
        private const val IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch"

        var unique_string = "unique_string"
        private const val IS_LOGIN1 = "IsLoggedIn1"
        const val IS_LOGIN = "IS_LOGIN"

        private lateinit var sharedPreferences: SharedPreferences
        private lateinit var editor: SharedPreferences.Editor
    }

    /*save the values in string*/
    public fun saveString(key: String, value: String?) {
        editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.commit()
    }

    /* get the string data with key*/
    public fun getSavedString(key: String?): String {
        return sharedPreferences.getString(key, "")!!
    }

    /*save the boolean value*/
    public fun saveBoolean(key: String, value: Boolean) {
        editor.putBoolean(key, value)
        editor.commit()
    }

    /* get the string data with key*/
    public fun getSavedBoolean(key: String): Boolean {
        return sharedPreferences.getBoolean(key, false)
    }

    /*save the Int value*/
    public fun saveInteger(key: String, value: Int) {
        editor.putInt(key, value)
        editor.commit()
    }

    /* get the int value with key*/
    public fun getSavedInteger(key: String): Int {
        return sharedPreferences.getInt(key, -1)
    }
    /* REMOVING THE APP DATA*/

    fun clearAppData() {
        editor.remove(ACCESS_TOKEN)
        editor.clear()
        editor.commit()
    }

    fun getToken(): String {
        return sharedPreferences.getString(ACCESS_TOKEN, "")!!
    }

    fun saveAccessToken(token : String) {
        editor = sharedPreferences.edit()
        editor.putString(ACCESS_TOKEN, token)
        editor.commit()
    }


}