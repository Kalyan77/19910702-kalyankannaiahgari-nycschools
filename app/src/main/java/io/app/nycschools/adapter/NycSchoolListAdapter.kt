package com.rideplus.partner.ui.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.app.nycschools.Models.NycSchoolsModel
import io.app.nycschools.databinding.ItemCustomerListBinding
import kotlinx.android.synthetic.main.item_customer_list.view.*

class NycSchoolListAdapter(
    var list: List<NycSchoolsModel>,
    var itemSelectionListener: ItemSelectionListener,
    var context: Context
) :
    RecyclerView.Adapter<NycSchoolListAdapter.CheckHolder>() {

    var binding: ItemCustomerListBinding? = null

    interface ItemSelectionListener {
        fun onItemClick(position: Int, rideList: NycSchoolsModel)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CheckHolder {
        binding =
            ItemCustomerListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CheckHolder(binding!!)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: CheckHolder, position: Int) {
        holder.setData(list[position])
    }

    inner class CheckHolder(var itemBinding: ItemCustomerListBinding) :
        RecyclerView.ViewHolder(itemBinding.root) {

        @SuppressLint("SetTextI18n")
        fun setData(model: NycSchoolsModel) {
            itemBinding.txtSchoolName.text = model.schoolName
            itemBinding.txtDbn.text = model.dbn
            itemBinding.txtSchoolEmail.text = "Email : " + model.schoolEmail
            itemBinding.txtSchoolPhone.text = "Phone : " + model.phoneNumber
            itemBinding.txtSchoolFax.text = "Fax : " + model.faxNumber
            itemBinding.txtSchoolAddress.text =
                "Address : " + model.primaryAddressLine1 + " , " + model.city + " , " + model.stateCode + " , " + model.zip
            itemBinding.txtSchoolWeburl.text = model.website

            itemView.setOnClickListener {
                itemSelectionListener.onItemClick(position, model)
                notifyDataSetChanged()
            }

        }
    }

}