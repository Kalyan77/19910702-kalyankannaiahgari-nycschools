package io.app.nycschools.activties

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.rideplus.partner.ui.adapters.NycSchoolListAdapter
import io.app.nycschools.Models.NycSchoolsModel
import io.app.nycschools.base.BaseActivity
import io.app.nycschools.databinding.ActivityNycListBinding
import io.app.nycschools.viewmodel.HomeViewModel
import kotlinx.android.synthetic.main.activity_nyc_list.*


class NycSchoolsListActivity : BaseActivity() {

    private lateinit var binding: ActivityNycListBinding
    lateinit var viewModel: HomeViewModel
    private var linearLayoutManager1: LinearLayoutManager? = null
    private var nycSchoolListAdapter: NycSchoolListAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityNycListBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this, appFactory).get(HomeViewModel::class.java)

        setSupportActionBar(my_toolbar)
        getSupportActionBar()!!.setDisplayShowHomeEnabled(true)

    }


    override fun onResume() {
        super.onResume()
        getSchoolsList()
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    fun getSchoolsList() {

        nodata.visibility = View.GONE
        binding.progressBar.visibility = View.VISIBLE
        viewModel.getSchoolsList()
            .observe(this,
                Observer {
                    if (it.size > 0) {
                        binding!!.rvNycSchools.itemAnimator = DefaultItemAnimator()
                        linearLayoutManager1 = LinearLayoutManager(this)
                        binding!!.rvNycSchools.setLayoutManager(linearLayoutManager1)
                        nodata.visibility = View.GONE
                        bindAdapterCustomerList(it)
                        binding.progressBar.visibility = View.GONE
                    } else {
                        nodata.visibility = View.VISIBLE
                        binding.progressBar.visibility = View.GONE
                    }
                })

    }


    private fun bindAdapterCustomerList(
        nycSchoolsModel: List<NycSchoolsModel>
    ) {
        if (nycSchoolsModel != null) {
            binding!!.rvNycSchools.visibility = View.VISIBLE
            nycSchoolListAdapter = NycSchoolListAdapter(
                nycSchoolsModel!!,
                object : NycSchoolListAdapter.ItemSelectionListener {
                    override fun onItemClick(
                        position: Int,
                        customerList: NycSchoolsModel
                    ) {
                        val intent = Intent(
                            applicationContext,
                            NycSchoolDataActivity::class.java
                        ).putExtra(
                            "school_data",
                            nycSchoolsModel.get(position)
                        )
                        startActivity(intent)
                    }
                }, applicationContext
            )
            binding!!.rvNycSchools.adapter = nycSchoolListAdapter
        } else {
            binding!!.rvNycSchools.visibility = View.GONE
        }

    }


}

