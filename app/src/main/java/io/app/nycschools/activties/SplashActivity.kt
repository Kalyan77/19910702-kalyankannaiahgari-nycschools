package io.app.nycschools.activties

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.Window
import android.view.WindowManager
import io.app.nycschools.CheckNetworkConnection
import io.app.nycschools.R
import io.app.nycschools.base.BaseActivity
import io.app.nycschools.util.PreferenceManagerNew


class SplashActivity : BaseActivity() {
    private var context: Context? = null
    var mobile: String? = null
    var loggedInAs = ""
    var logged_r_not = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED
        setContentView(R.layout.activity_splash)
        transparentToolbar()

        context = applicationContext

    }

    override fun onResume() {
        super.onResume()
        if (CheckNetworkConnection.isOnline(this@SplashActivity)) {

            mobile = PreferenceManagerNew.mobile
            logged_r_not = PreferenceManagerNew.islogin
            loggedInAs = PreferenceManagerNew.loggedInAs

            Handler().postDelayed({
                launchHomeScreen()
                finish()
            }, SPLASH_TIME_OUT.toLong())
        } else {
            showAlertDialogForSplash()
        }
    }

    private fun launchHomeScreen() {

                var intent = Intent(
                    applicationContext,
                    NycSchoolsListActivity::class.java
                )
                startActivity(intent)
                finish()

    }

    override fun onStart() {
        super.onStart()
    }

    private fun transparentToolbar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true)
        }
        if (Build.VERSION.SDK_INT >= 19) {
            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false)
            window.statusBarColor = Color.TRANSPARENT
        }
    }

    open fun setWindowFlag(activity: Activity, bits: Int, on: Boolean) {
        val win = activity.window
        val winParams = win.attributes
        if (on) {
            winParams.flags = winParams.flags or bits
        } else {
            winParams.flags = winParams.flags and bits.inv()
        }
        win.attributes = winParams
    }

    companion object {
        private const val SPLASH_TIME_OUT = 1000
        private val TAG = SplashActivity::class.java.simpleName
    }
}