package io.app.nycschools.activties

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.ui.AppBarConfiguration
import io.app.nycschools.Models.NycSchoolsModel
import io.app.nycschools.base.BaseActivity
import io.app.nycschools.databinding.ActivityNycSchoolDataBinding
import io.app.nycschools.viewmodel.HomeViewModel

class NycSchoolDataActivity : BaseActivity() {

    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var binding: ActivityNycSchoolDataBinding
    lateinit var viewModel: HomeViewModel

    var dbn: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityNycSchoolDataBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel = ViewModelProvider(this, appFactory).get(HomeViewModel::class.java)
        setSupportActionBar(binding.myToolbar)


        if (intent.extras != null && intent.extras!!.containsKey("school_data")) {
            val model: NycSchoolsModel? =
                intent.getSerializableExtra("school_data") as NycSchoolsModel?
            if (model != null) {

                dbn = model.dbn
                binding.txtSchoolName.text = "School Name : " + "\n" + model.schoolName
                binding.txtSchoolOverview.text = model.overviewParagraph
                if(model.languageClasses!=null)
                binding.txtLanguageClasses.text = "Teaching Languages : " + model.languageClasses
                else
                    binding.txtLanguageClasses.visibility=View.GONE
                if(model.schoolSports!=null)
                binding.txtSchoolSports.text = "Sports : " + model.schoolSports
                else
                    binding.txtSchoolSports.visibility=View.GONE
                if(model.dbn!=null)
                binding.txtDbn.text = model.dbn
                else
                    binding.txtDbn.visibility=View.GONE
                binding.txtSchoolEmail.text = "Email : " + model.schoolEmail
                binding.txtSchoolPhone.text = "Phone : " + model.phoneNumber
                binding.txtSchoolFax.text = "Fax : " + model.faxNumber
                binding.txtSchoolAddress.text =
                    "Address : " + model.primaryAddressLine1 + " , " + model.city + " , " + model.stateCode + " , " + model.zip
                binding.txtSchoolWeburl.text = model.website

            }
        }


        getSatData()

    }

    fun getSatData() {

        binding.progressBar.visibility = View.VISIBLE
        viewModel.getSatData()
            .observe(this,
                Observer {
                    if (it.size > 0) {
                        for (i in it.iterator()) {
                            if (i.dbn.equals(dbn)) {
                                binding.satLayout.visibility=View.VISIBLE
                                binding.txtSatTestTakers.text = "Num of SAT Test Takers : " + i.numOfSatTestTakers
                                binding.txtReadingAvg.text = "SAT Critical Reading Avg. Score : " + i.satCriticalReadingAvgScore
                                binding.txtReadingMathAvg.text = "SAT Math Avg. Score : " + i.satMathAvgScore
                                binding.txtReadingWritingAvg.text = "SAT Writing Avg. Score : " + i.satWritingAvgScore
                            }

                        }
                        binding.progressBar.visibility = View.GONE

                    } else {
                        binding.progressBar.visibility = View.GONE
                    }
                })

    }


}