package io.app.nycschools.activties

import android.app.ActivityManager
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.SystemClock
import android.provider.Settings
import android.text.TextUtils
import android.view.*
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.DimenRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.recyclerview.widget.RecyclerView.OnItemTouchListener
import io.app.nycschools.R
import io.app.nycschools.repositories.ClickListener
import java.util.*

open class BaseActivity : AppCompatActivity() {
    var alertDialog: AlertDialog? = null
    var dialog: Dialog? = null
    var isShown = false
        private set
    private var mLastClickTime: Long = 0
    var listAll = ArrayList<String>()

    override fun onResume() {
        super.onResume()
        isShown = true
    }

    override fun onPause() {
        super.onPause()
        isShown = false
    }

    fun showToast(context: Context?, message: String?) {
        if (context != null && !TextUtils.isEmpty(message)) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun showAlertDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("No internet Connection")
        builder.setMessage("Please turn on internet connection to continue")
        builder.setCancelable(false)
        builder.setPositiveButton(
            "settings",
            DialogInterface.OnClickListener { dialog, id ->
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return@OnClickListener
                }
                mLastClickTime = SystemClock.elapsedRealtime()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val intent =
                        Intent(Settings.ACTION_SETTINGS)
                    startActivity(intent)
                }
            })
            .setNegativeButton("close") { dialog, which -> dialog.dismiss() }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    fun showAlertDialogForSplash() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("No internet Connection")
        builder.setMessage("Please turn on internet connection to continue")
        builder.setCancelable(false)
        builder.setPositiveButton(
            "settings",
            DialogInterface.OnClickListener { dialog, id ->
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return@OnClickListener
                }
                mLastClickTime = SystemClock.elapsedRealtime()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val intent =
                        Intent(Settings.ACTION_SETTINGS)
                    startActivity(intent)
                }
            })
            .setNegativeButton("close") { dialog, which ->
                dialog.dismiss()
                finish()
            }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    fun showAlertDialogForSuccess(s: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("Success")
        builder.setCancelable(false)
        builder.setMessage(s)
        builder.setNegativeButton(
            "close"
        ) { dialog, which ->
            //startActivity(new Intent(getApplicationContext(), BarcodeScannerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            dialog.dismiss()
        }
        val alertDialog = builder.create()
        alertDialog.show()
        //  alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.lightgreen)));
    }

    fun showAlertDialogForNoData(s: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("No Data")
        builder.setCancelable(false)
        builder.setMessage(s)
        builder.setNegativeButton(
            "close"
        ) { dialog, which ->
            //startActivity(new Intent(getApplicationContext(), BarcodeScannerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            dialog.dismiss()
        }
        val alertDialog = builder.create()
        alertDialog.show()
        //  alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.lightgreen)));
    }

    inner class ItemOffsetDecoration(private val mItemOffset: Int) : ItemDecoration() {

        constructor(context: Context, @DimenRes itemOffsetId: Int) : this(
            context.resources.getDimensionPixelSize(
                itemOffsetId
            )
        ) {
        }

        override fun getItemOffsets(
            outRect: Rect, view: View, parent: RecyclerView,
            state: RecyclerView.State
        ) {
            super.getItemOffsets(outRect, view, parent, state)
            outRect[mItemOffset, mItemOffset, mItemOffset] = mItemOffset
        }

    }

    internal inner class RecyclerTouchListener(
        context: Context?,
        recycleView: RecyclerView,
        private val clicklistener: ClickListener?
    ) : OnItemTouchListener {
        private val gestureDetector: GestureDetector
        override fun onInterceptTouchEvent(
            rv: RecyclerView,
            e: MotionEvent
        ): Boolean {
            val child = rv.findChildViewUnder(e.x, e.y)
            if (child != null && clicklistener != null && gestureDetector.onTouchEvent(e)) {
                clicklistener.onClick(child, rv.getChildAdapterPosition(child))
            }
            return false
        }

        override fun onTouchEvent(
            rv: RecyclerView,
            e: MotionEvent
        ) {
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {}

        init {
            gestureDetector = GestureDetector(
                context,
                object : GestureDetector.SimpleOnGestureListener() {
                    override fun onSingleTapUp(e: MotionEvent): Boolean {
                        return true
                    }

                    override fun onLongPress(e: MotionEvent) {
                        val child =
                            recycleView.findChildViewUnder(e.x, e.y)
                        if (child != null && clicklistener != null) {
                            clicklistener.onLongClick(
                                child,
                                recycleView.getChildAdapterPosition(child)
                            )
                        }
                    }
                })
        }
    }

    fun showProgress() {
        if (!isShown) {
            val pbar = ProgressBar(this)
            pbar.setBackgroundColor(resources.getColor(R.color.bg_color))
            dialog = Dialog(this)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setContentView(pbar)
            dialog!!.setCancelable(false)
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.show()
        }
    }

    fun closeProgress() {
        if (isShown && dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
            dialog!!.cancel()
        }
    }


    companion object {
        fun isServiceRunning(
            context: Context,
            serviceClass: Class<*>
        ): Boolean {
            val manager =
                context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(
                Int.MAX_VALUE
            )) {
                if (serviceClass.name == service.service.className) {
                    return true
                }
            }
            return false
        }

        fun getUniqueID(context: Context): String? {
            return try {
                var myAndroidDeviceId: String? = ""
                // TODO : imei is not being received when phone app is crashed
                myAndroidDeviceId = Settings.Secure.getString(
                    context.contentResolver,
                    Settings.Secure.ANDROID_ID
                )
                //L.info("Device Id : " + myAndroidDeviceId);
                myAndroidDeviceId
            } catch (e: Exception) { //L.error(e.getMessage(), e);
                null
            }
        }
    }

    fun commonDialog(context:Context,layoutDialog: Int, isCancelable: Boolean) =
        Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(isCancelable)
            setCanceledOnTouchOutside(isCancelable)
            setContentView(layoutDialog)
            val window = window!!
            val param = window.attributes
            param.gravity = Gravity.CENTER or Gravity.CENTER_HORIZONTAL
            param.windowAnimations =
                R.style.AppTheme
            window.attributes = param
            window.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        application,
                        R.color.alert_dialog
                    )
                )
            )
            window.setLayout(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
        }

}