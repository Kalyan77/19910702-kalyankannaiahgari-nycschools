package io.app.nycschools.Models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NycSatModel {

    @SerializedName("dbn")
    @Expose
    public String dbn;
    @SerializedName("school_name")
    @Expose
    public String schoolName;
    @SerializedName("num_of_sat_test_takers")
    @Expose
    public String numOfSatTestTakers;
    @SerializedName("sat_critical_reading_avg_score")
    @Expose
    public String satCriticalReadingAvgScore;
    @SerializedName("sat_math_avg_score")
    @Expose
    public String satMathAvgScore;
    @SerializedName("sat_writing_avg_score")
    @Expose
    public String satWritingAvgScore;

}
