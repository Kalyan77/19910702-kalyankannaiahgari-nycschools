package io.app.nycschools.restapi

import io.reactivex.Observable
import io.app.nycschools.Models.NycSatModel
import io.app.nycschools.Models.NycSchoolsModel
import retrofit2.http.*
import java.util.*


interface ApiService {

    @GET(API.nucSchools)
     fun getSchoolsList(): Observable<List<NycSchoolsModel>>

    @GET(API.satData)
     fun getSatDate(): Observable<List<NycSatModel>>

}