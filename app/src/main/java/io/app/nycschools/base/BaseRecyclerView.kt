package io.app.nycschools.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerView : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var itemClickListener: ItemClickListener

    fun setListener(itemClickListener: ItemClickListener) {
        this.itemClickListener = itemClickListener
    }
    interface ItemClickListener {
        fun onItemClicked(model: BaseModel)
    }
}