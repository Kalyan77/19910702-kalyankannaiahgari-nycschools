package io.app.nycschools.base

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import io.app.nycschools.util.Utils.Companion.logInfo
import dagger.android.support.AndroidSupportInjection
import io.app.nycschools.viewmodel.HomeViewModel
import javax.inject.Inject

open class BaseFragment : Fragment() {

//    lateinit var preferenceManager : PreferenceManager
    @Inject
    lateinit var appfactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        Log.v("PRK","onCreate()")
        AndroidSupportInjection.inject(this)
        super.onCreate(savedInstanceState)

        val viewModel = ViewModelProvider(this,appfactory).get(HomeViewModel::class.java)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onPause() {
        super.onPause()
        Log.v("PRK","onPause()")
    }

    override fun onResume() {
        super.onResume()
        Log.v("PRK","onResume()")
    }

    override fun onStart() {
        super.onStart()
        Log.v("PRK","onStart()")
    }

    override fun onStop() {
        super.onStop()
        Log.v("PRK","onStop()")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.v("PRK","onDestroy()")
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Log.v("PRK","onDestroyView()")
    }

    override fun onDetach() {
        super.onDetach()
        Log.v("PRK","onDetach()")
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Log.v("PRK","onAttach()")
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        Log.v("PRK","onActivityCreated()")

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        logInfo("data","test")
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
}