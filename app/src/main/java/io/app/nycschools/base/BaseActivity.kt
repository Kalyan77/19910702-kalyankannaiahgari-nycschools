package io.app.nycschools.base

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.provider.Settings
import android.os.Bundle
import android.os.PersistableBundle
import android.os.SystemClock
import android.text.TextUtils
import android.view.Gravity
import android.view.Window
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import io.app.nycschools.R
import io.app.nycschools.factory.Injectable
import javax.inject.Inject
import dagger.android.HasActivityInjector
import io.app.nycschools.viewmodel.HomeViewModel

open class BaseActivity : AppCompatActivity(), HasActivityInjector, HasSupportFragmentInjector,
    Injectable {

    lateinit var baseHomeViewModle: HomeViewModel

    @Inject
    lateinit var appFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        AndroidInjection.inject(this)
        baseHomeViewModle = ViewModelProvider(this, appFactory).get(HomeViewModel::class.java)
    }

    var dialog: Dialog? = null
    var isShown = false
        private set
    private var mLastClickTime: Long = 0

    override fun onResume() {
        super.onResume()
        isShown = true
    }

    override fun onPause() {
        super.onPause()
        isShown = false
    }

    fun showToast(context: Context?, message: String?) {
        if (context != null && !TextUtils.isEmpty(message)) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT)
                .show()
        }
    }

    fun showAlertDialogForSplash() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("No internet Connection")
        builder.setMessage("Please turn on internet connection to continue")
        builder.setCancelable(false)
        builder.setPositiveButton(
            "settings",
            DialogInterface.OnClickListener { dialog, id ->
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return@OnClickListener
                }
                mLastClickTime = SystemClock.elapsedRealtime()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val intent =
                        Intent(Settings.ACTION_SETTINGS)
                    startActivity(intent)
                }
            })
            .setNegativeButton("close") { dialog, which ->
                dialog.dismiss()
                finish()
            }
        val alertDialog = builder.create()
        alertDialog.show()
    }

    fun showProgress() {
        if (!isShown) {
            val pbar = ProgressBar(this)
            pbar.setBackgroundColor(resources.getColor(R.color.transparent))
            dialog = Dialog(this)
            dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog!!.setContentView(pbar)
            dialog!!.setCancelable(false)
            dialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog!!.show()
        }
    }

    fun closeProgress() {
        if (isShown && dialog != null && dialog!!.isShowing) {
            dialog!!.dismiss()
            dialog!!.cancel()
        }
    }

    fun commonDialog(context: Context, layoutDialog: Int, isCancelable: Boolean) =
        Dialog(context).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            setCancelable(isCancelable)
            setCanceledOnTouchOutside(isCancelable)
            setContentView(layoutDialog)
            val window = window!!
            val param = window.attributes
            param.gravity = Gravity.CENTER or Gravity.CENTER_HORIZONTAL
            param.windowAnimations =
                R.style.AppTheme
            window.attributes = param
            window.setBackgroundDrawable(
                ColorDrawable(
                    ContextCompat.getColor(
                        application,
                        R.color.alert_dialog
                    )
                )
            )
            window.setLayout(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT
            )
        }

    fun showAlertDialogForNoData(s: String?) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("No Data")
        builder.setCancelable(false)
        builder.setMessage(s)
        builder.setNegativeButton(
            "close"
        ) { dialog, which ->
            //startActivity(new Intent(getApplicationContext(), BarcodeScannerActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
            dialog.dismiss()
        }
        val alertDialog = builder.create()
        alertDialog.show()
        //  alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.lightgreen)));
    }

    @Inject
    internal lateinit var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    @Inject
    internal lateinit var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector
    }

    @Inject
    lateinit var HomeViewModel: HomeViewModel
    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector
    }


    override fun onStart() {
        super.onStart()
    }


    fun showDialog(message: String, cancellable: Boolean) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(getString(R.string.app_name))
        builder.setCancelable(cancellable)
        builder.setMessage(message)
        builder.setPositiveButton(
            R.string.ok
        ) { dialog, _ -> dialog.dismiss() }
        builder.show()
    }


}