package io.app.nycschools.base

import com.google.gson.annotations.SerializedName

open class BaseModel {
    @SerializedName("created_at")
    lateinit var createdAt: String
    lateinit var updateAt: String
    lateinit var message: String
    lateinit var status: String
}