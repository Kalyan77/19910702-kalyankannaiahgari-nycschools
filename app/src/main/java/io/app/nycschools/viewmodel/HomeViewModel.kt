package io.app.nycschools.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.app.nycschools.Models.*
import io.app.nycschools.repositories.HomeRepository
import javax.inject.Inject

class HomeViewModel @Inject constructor(var homeRepository: HomeRepository) : ViewModel() {

    fun getSchoolsList(): MutableLiveData<List<NycSchoolsModel>> {
        return homeRepository.getSchoolsList()
    }

    fun getSatData(): MutableLiveData<List<NycSatModel>> {
        return homeRepository.getSatDate()
    }

}




