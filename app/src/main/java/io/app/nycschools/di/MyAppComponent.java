package io.app.nycschools.di;


import android.app.Application;

import io.app.nycschools.di.module.ActivityContributorsModule;
import io.app.nycschools.di.module.AppModule;
import io.app.nycschools.di.module.RepositoryModule;
import io.app.nycschools.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.support.AndroidSupportInjectionModule;
import io.app.nycschools.AppApplication;

@Singleton
@Component(
        modules = {
                AppModule.class,
                ViewModelModule.class,
                RepositoryModule.class,
                AndroidSupportInjectionModule.class,
                AndroidInjectionModule.class,
                ActivityContributorsModule.class
        }

)
public interface MyAppComponent {

    @Component.Builder
    interface Builder {

        @BindsInstance
        Builder application(Application application);

        MyAppComponent build();

    }


    public void inject(AppApplication app);

}


//@Singleton
//@Component(
//        modules = {
//                NetworkModule.class
//        }
//)
//public interface MyAppComponent {
//    @Component.Builder
//    interface Builder {
//        Builder networkModule(NetworkModule module);
//        AppComponent build();
//    }
//    void inject(MyActivity inject);
//}
