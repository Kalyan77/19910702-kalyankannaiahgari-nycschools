package io.app.nycschools.di.module


import io.app.nycschools.repositories.*
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class RepositoryModule {

    @Provides
    fun providesHomeRepository(retrofit: Retrofit): HomeRepository {
        return HomeRepository(retrofit)
    }

//    @Provides
//    fun providesLoginRepository(retrofit: Retrofit): LoginRepository {
//        return LoginRepository(retrofit)
//    }

//    @Provides
//    fun providesSearchRepository(retrofit: Retrofit): SearchRepository {
//        return SearchRepository(retrofit)
//    }

//    @Provides
//    fun walletRepository(retrofit: Retrofit): WalletRepository {
//        return WalletRepository(retrofit)
//    }


}