package io.app.nycschools.di.module


import io.app.nycschools.base.BaseActivity
import io.app.nycschools.base.BaseFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.app.nycschools.activties.*

@Module
abstract class ActivityContributorsModule {

    @ContributesAndroidInjector
    abstract fun baseActivity(): BaseActivity



    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity


    @ContributesAndroidInjector
    abstract fun nycSchoolsListActivity(): NycSchoolsListActivity


    @ContributesAndroidInjector
    abstract fun salesPersonsNycSchoolDataActivity(): NycSchoolDataActivity


    //Fragment
    @ContributesAndroidInjector
    abstract fun baseFragmentFragment(): BaseFragment


}