package io.app.nycschools.di

import io.app.nycschools.di.module.AppModule
import io.app.nycschools.AppApplication
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, AndroidInjectionModule::class, AndroidSupportInjectionModule::class])
interface AppComponent {

    fun inject(app: AppApplication)
}