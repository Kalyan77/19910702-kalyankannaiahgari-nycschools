package io.app.nycschools.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.kotlinapplication.factory.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import io.app.nycschools.factory.AppViewModelProvider
import io.app.nycschools.viewmodel.HomeViewModel

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(homre: HomeViewModel): ViewModel

//    @Binds
//    @IntoMap
//    @ViewModelKey(LoginViewModel::class)
//    abstract fun bindLoginViewModel(loginViewModel: LoginViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: AppViewModelProvider): ViewModelProvider.Factory


/*
* inject the appication with @Inject constructur
* bind the view model in viewmodel Module
* add the activity in activity builder
* the the repository in repository module with required consructore parameters
* */
}