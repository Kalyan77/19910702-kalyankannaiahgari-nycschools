package io.app.nycschools

import android.app.Activity
import android.app.Application
import android.app.Fragment
import androidx.lifecycle.*
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import dagger.android.HasFragmentInjector
import io.app.nycschools.di.AppInject
import io.app.nycschools.util.PreferenceManagerNew
import kotlinx.coroutines.*
import javax.inject.Inject

class AppApplication : Application(), HasActivityInjector,
    HasFragmentInjector, LifecycleObserver {

    @JvmField
    @Inject
    var activityDispatchingAndroidInjector: DispatchingAndroidInjector<Activity>? = null

    @JvmField
    @Inject
    var fragmentDispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>? = null
    private lateinit var job: Job
    override fun activityInjector(): AndroidInjector<Activity> {
        return activityDispatchingAndroidInjector!!
    }

    override fun fragmentInjector(): AndroidInjector<Fragment> {
        return fragmentDispatchingAndroidInjector!!
    }

    override fun onCreate() {
        super.onCreate()
        AppInject.init(this)
        PreferenceManagerNew.init(context = this)
        runBlocking {
            launch {

            }
        }
    }

}
