package io.app.nycschools.repositories

import android.util.Log
import androidx.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.app.nycschools.Models.NycSatModel
import io.app.nycschools.Models.NycSchoolsModel
import io.app.nycschools.restapi.ApiService
import retrofit2.Retrofit
import javax.inject.Inject

class HomeRepository @Inject constructor(var retrofit: Retrofit) {

    private lateinit var nycSchoolsModel: MutableLiveData<List<NycSchoolsModel>>
    private lateinit var nycSatModel: MutableLiveData<List<NycSatModel>>

    fun getSchoolsList(): MutableLiveData<List<NycSchoolsModel>> {
        nycSchoolsModel = MutableLiveData()
        val apiService = retrofit.create(ApiService::class.java)
        apiService.getSchoolsList().subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                nycSchoolsModel.postValue(it)
            }, {
                Log.v("response user", "reseponse : $it")
            })
        return nycSchoolsModel
    }

    fun getSatDate(): MutableLiveData<List<NycSatModel>> {
        nycSatModel = MutableLiveData()
        val apiService = retrofit.create(ApiService::class.java)
        apiService.getSatDate().subscribeOn(Schedulers.io())
            .unsubscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                nycSatModel.postValue(it)
            }, {
                Log.v("response user", "reseponse : $it")
            })
        return nycSatModel
    }




}
