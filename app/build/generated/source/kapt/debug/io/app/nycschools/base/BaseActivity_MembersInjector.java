// Generated by Dagger (https://google.github.io/dagger).
package io.app.nycschools.base;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import dagger.MembersInjector;
import dagger.android.DispatchingAndroidInjector;
import io.app.nycschools.viewmodel.HomeViewModel;
import javax.inject.Provider;

public final class BaseActivity_MembersInjector implements MembersInjector<BaseActivity> {
  private final Provider<ViewModelProvider.Factory> appFactoryProvider;

  private final Provider<DispatchingAndroidInjector<Activity>>
      activityDispatchingAndroidInjectorProvider;

  private final Provider<DispatchingAndroidInjector<Fragment>>
      fragmentDispatchingAndroidInjectorProvider;

  private final Provider<HomeViewModel> homeViewModelProvider;

  public BaseActivity_MembersInjector(
      Provider<ViewModelProvider.Factory> appFactoryProvider,
      Provider<DispatchingAndroidInjector<Activity>> activityDispatchingAndroidInjectorProvider,
      Provider<DispatchingAndroidInjector<Fragment>> fragmentDispatchingAndroidInjectorProvider,
      Provider<HomeViewModel> homeViewModelProvider) {
    this.appFactoryProvider = appFactoryProvider;
    this.activityDispatchingAndroidInjectorProvider = activityDispatchingAndroidInjectorProvider;
    this.fragmentDispatchingAndroidInjectorProvider = fragmentDispatchingAndroidInjectorProvider;
    this.homeViewModelProvider = homeViewModelProvider;
  }

  public static MembersInjector<BaseActivity> create(
      Provider<ViewModelProvider.Factory> appFactoryProvider,
      Provider<DispatchingAndroidInjector<Activity>> activityDispatchingAndroidInjectorProvider,
      Provider<DispatchingAndroidInjector<Fragment>> fragmentDispatchingAndroidInjectorProvider,
      Provider<HomeViewModel> homeViewModelProvider) {
    return new BaseActivity_MembersInjector(
        appFactoryProvider,
        activityDispatchingAndroidInjectorProvider,
        fragmentDispatchingAndroidInjectorProvider,
        homeViewModelProvider);
  }

  @Override
  public void injectMembers(BaseActivity instance) {
    injectAppFactory(instance, appFactoryProvider.get());
    injectActivityDispatchingAndroidInjector(
        instance, activityDispatchingAndroidInjectorProvider.get());
    injectFragmentDispatchingAndroidInjector(
        instance, fragmentDispatchingAndroidInjectorProvider.get());
    injectHomeViewModel(instance, homeViewModelProvider.get());
  }

  public static void injectAppFactory(BaseActivity instance, ViewModelProvider.Factory appFactory) {
    instance.appFactory = appFactory;
  }

  public static void injectActivityDispatchingAndroidInjector(
      BaseActivity instance,
      DispatchingAndroidInjector<Activity> activityDispatchingAndroidInjector) {
    instance.activityDispatchingAndroidInjector = activityDispatchingAndroidInjector;
  }

  public static void injectFragmentDispatchingAndroidInjector(
      BaseActivity instance,
      DispatchingAndroidInjector<Fragment> fragmentDispatchingAndroidInjector) {
    instance.fragmentDispatchingAndroidInjector = fragmentDispatchingAndroidInjector;
  }

  public static void injectHomeViewModel(BaseActivity instance, HomeViewModel HomeViewModel) {
    instance.HomeViewModel = HomeViewModel;
  }
}
