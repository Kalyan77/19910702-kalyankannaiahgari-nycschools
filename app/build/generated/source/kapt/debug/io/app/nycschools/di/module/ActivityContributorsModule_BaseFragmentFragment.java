package io.app.nycschools.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import io.app.nycschools.base.BaseFragment;

@Module(
  subcomponents = ActivityContributorsModule_BaseFragmentFragment.BaseFragmentSubcomponent.class
)
public abstract class ActivityContributorsModule_BaseFragmentFragment {
  private ActivityContributorsModule_BaseFragmentFragment() {}

  @Binds
  @IntoMap
  @ClassKey(BaseFragment.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      BaseFragmentSubcomponent.Builder builder);

  @Subcomponent
  public interface BaseFragmentSubcomponent extends AndroidInjector<BaseFragment> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<BaseFragment> {}
  }
}
