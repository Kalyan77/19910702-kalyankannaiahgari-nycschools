package io.app.nycschools.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import io.app.nycschools.base.BaseActivity;

@Module(subcomponents = ActivityContributorsModule_BaseActivity.BaseActivitySubcomponent.class)
public abstract class ActivityContributorsModule_BaseActivity {
  private ActivityContributorsModule_BaseActivity() {}

  @Binds
  @IntoMap
  @ClassKey(BaseActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      BaseActivitySubcomponent.Builder builder);

  @Subcomponent
  public interface BaseActivitySubcomponent extends AndroidInjector<BaseActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<BaseActivity> {}
  }
}
