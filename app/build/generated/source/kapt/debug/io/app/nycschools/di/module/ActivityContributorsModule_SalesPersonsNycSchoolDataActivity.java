package io.app.nycschools.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import io.app.nycschools.activties.NycSchoolDataActivity;

@Module(
  subcomponents =
      ActivityContributorsModule_SalesPersonsNycSchoolDataActivity.NycSchoolDataActivitySubcomponent
          .class
)
public abstract class ActivityContributorsModule_SalesPersonsNycSchoolDataActivity {
  private ActivityContributorsModule_SalesPersonsNycSchoolDataActivity() {}

  @Binds
  @IntoMap
  @ClassKey(NycSchoolDataActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      NycSchoolDataActivitySubcomponent.Builder builder);

  @Subcomponent
  public interface NycSchoolDataActivitySubcomponent
      extends AndroidInjector<NycSchoolDataActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<NycSchoolDataActivity> {}
  }
}
