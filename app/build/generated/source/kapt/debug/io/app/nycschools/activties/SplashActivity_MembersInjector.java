// Generated by Dagger (https://google.github.io/dagger).
package io.app.nycschools.activties;

import android.app.Activity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import dagger.MembersInjector;
import dagger.android.DispatchingAndroidInjector;
import io.app.nycschools.base.BaseActivity_MembersInjector;
import io.app.nycschools.viewmodel.HomeViewModel;
import javax.inject.Provider;

public final class SplashActivity_MembersInjector implements MembersInjector<SplashActivity> {
  private final Provider<ViewModelProvider.Factory> appFactoryProvider;

  private final Provider<DispatchingAndroidInjector<Activity>>
      activityDispatchingAndroidInjectorProvider;

  private final Provider<DispatchingAndroidInjector<Fragment>>
      fragmentDispatchingAndroidInjectorProvider;

  private final Provider<HomeViewModel> homeViewModelProvider;

  public SplashActivity_MembersInjector(
      Provider<ViewModelProvider.Factory> appFactoryProvider,
      Provider<DispatchingAndroidInjector<Activity>> activityDispatchingAndroidInjectorProvider,
      Provider<DispatchingAndroidInjector<Fragment>> fragmentDispatchingAndroidInjectorProvider,
      Provider<HomeViewModel> homeViewModelProvider) {
    this.appFactoryProvider = appFactoryProvider;
    this.activityDispatchingAndroidInjectorProvider = activityDispatchingAndroidInjectorProvider;
    this.fragmentDispatchingAndroidInjectorProvider = fragmentDispatchingAndroidInjectorProvider;
    this.homeViewModelProvider = homeViewModelProvider;
  }

  public static MembersInjector<SplashActivity> create(
      Provider<ViewModelProvider.Factory> appFactoryProvider,
      Provider<DispatchingAndroidInjector<Activity>> activityDispatchingAndroidInjectorProvider,
      Provider<DispatchingAndroidInjector<Fragment>> fragmentDispatchingAndroidInjectorProvider,
      Provider<HomeViewModel> homeViewModelProvider) {
    return new SplashActivity_MembersInjector(
        appFactoryProvider,
        activityDispatchingAndroidInjectorProvider,
        fragmentDispatchingAndroidInjectorProvider,
        homeViewModelProvider);
  }

  @Override
  public void injectMembers(SplashActivity instance) {
    BaseActivity_MembersInjector.injectAppFactory(instance, appFactoryProvider.get());
    BaseActivity_MembersInjector.injectActivityDispatchingAndroidInjector(
        instance, activityDispatchingAndroidInjectorProvider.get());
    BaseActivity_MembersInjector.injectFragmentDispatchingAndroidInjector(
        instance, fragmentDispatchingAndroidInjectorProvider.get());
    BaseActivity_MembersInjector.injectHomeViewModel(instance, homeViewModelProvider.get());
  }
}
