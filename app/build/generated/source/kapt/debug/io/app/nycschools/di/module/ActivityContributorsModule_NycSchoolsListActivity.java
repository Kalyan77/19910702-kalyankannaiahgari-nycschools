package io.app.nycschools.di.module;

import dagger.Binds;
import dagger.Module;
import dagger.Subcomponent;
import dagger.android.AndroidInjector;
import dagger.multibindings.ClassKey;
import dagger.multibindings.IntoMap;
import io.app.nycschools.activties.NycSchoolsListActivity;

@Module(
  subcomponents =
      ActivityContributorsModule_NycSchoolsListActivity.NycSchoolsListActivitySubcomponent.class
)
public abstract class ActivityContributorsModule_NycSchoolsListActivity {
  private ActivityContributorsModule_NycSchoolsListActivity() {}

  @Binds
  @IntoMap
  @ClassKey(NycSchoolsListActivity.class)
  abstract AndroidInjector.Factory<?> bindAndroidInjectorFactory(
      NycSchoolsListActivitySubcomponent.Builder builder);

  @Subcomponent
  public interface NycSchoolsListActivitySubcomponent
      extends AndroidInjector<NycSchoolsListActivity> {
    @Subcomponent.Builder
    abstract class Builder extends AndroidInjector.Builder<NycSchoolsListActivity> {}
  }
}
