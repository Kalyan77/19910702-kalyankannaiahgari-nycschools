// Generated by Dagger (https://google.github.io/dagger).
package io.app.nycschools.repositories;

import dagger.internal.Factory;
import javax.inject.Provider;
import retrofit2.Retrofit;

public final class HomeRepository_Factory implements Factory<HomeRepository> {
  private final Provider<Retrofit> retrofitProvider;

  public HomeRepository_Factory(Provider<Retrofit> retrofitProvider) {
    this.retrofitProvider = retrofitProvider;
  }

  @Override
  public HomeRepository get() {
    return new HomeRepository(retrofitProvider.get());
  }

  public static HomeRepository_Factory create(Provider<Retrofit> retrofitProvider) {
    return new HomeRepository_Factory(retrofitProvider);
  }

  public static HomeRepository newHomeRepository(Retrofit retrofit) {
    return new HomeRepository(retrofit);
  }
}
