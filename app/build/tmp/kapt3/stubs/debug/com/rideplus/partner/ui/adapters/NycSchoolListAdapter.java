package com.rideplus.partner.ui.adapters;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0011\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\u0018\u00002\f\u0012\b\u0012\u00060\u0002R\u00020\u00000\u0001:\u0002\'(B#\u0012\f\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u0012\u0006\u0010\b\u001a\u00020\t\u00a2\u0006\u0002\u0010\nJ\b\u0010\u001d\u001a\u00020\u001eH\u0016J\u001c\u0010\u001f\u001a\u00020 2\n\u0010!\u001a\u00060\u0002R\u00020\u00002\u0006\u0010\"\u001a\u00020\u001eH\u0016J\u001c\u0010#\u001a\u00060\u0002R\u00020\u00002\u0006\u0010$\u001a\u00020%2\u0006\u0010&\u001a\u00020\u001eH\u0016R\u001c\u0010\u000b\u001a\u0004\u0018\u00010\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001a\u0010\b\u001a\u00020\tX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0011\u0010\u0012\"\u0004\b\u0013\u0010\u0014R\u001a\u0010\u0006\u001a\u00020\u0007X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R \u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0019\u0010\u001a\"\u0004\b\u001b\u0010\u001c\u00a8\u0006)"}, d2 = {"Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter$CheckHolder;", "list", "", "Lio/app/nycschools/Models/NycSchoolsModel;", "itemSelectionListener", "Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter$ItemSelectionListener;", "context", "Landroid/content/Context;", "(Ljava/util/List;Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter$ItemSelectionListener;Landroid/content/Context;)V", "binding", "Lio/app/nycschools/databinding/ItemCustomerListBinding;", "getBinding", "()Lio/app/nycschools/databinding/ItemCustomerListBinding;", "setBinding", "(Lio/app/nycschools/databinding/ItemCustomerListBinding;)V", "getContext", "()Landroid/content/Context;", "setContext", "(Landroid/content/Context;)V", "getItemSelectionListener", "()Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter$ItemSelectionListener;", "setItemSelectionListener", "(Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter$ItemSelectionListener;)V", "getList", "()Ljava/util/List;", "setList", "(Ljava/util/List;)V", "getItemCount", "", "onBindViewHolder", "", "holder", "position", "onCreateViewHolder", "parent", "Landroid/view/ViewGroup;", "viewType", "CheckHolder", "ItemSelectionListener", "app_debug"})
public final class NycSchoolListAdapter extends androidx.recyclerview.widget.RecyclerView.Adapter<com.rideplus.partner.ui.adapters.NycSchoolListAdapter.CheckHolder> {
    @org.jetbrains.annotations.NotNull()
    private java.util.List<? extends io.app.nycschools.Models.NycSchoolsModel> list;
    @org.jetbrains.annotations.NotNull()
    private com.rideplus.partner.ui.adapters.NycSchoolListAdapter.ItemSelectionListener itemSelectionListener;
    @org.jetbrains.annotations.NotNull()
    private android.content.Context context;
    @org.jetbrains.annotations.Nullable()
    private io.app.nycschools.databinding.ItemCustomerListBinding binding;
    
    public NycSchoolListAdapter(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends io.app.nycschools.Models.NycSchoolsModel> list, @org.jetbrains.annotations.NotNull()
    com.rideplus.partner.ui.adapters.NycSchoolListAdapter.ItemSelectionListener itemSelectionListener, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<io.app.nycschools.Models.NycSchoolsModel> getList() {
        return null;
    }
    
    public final void setList(@org.jetbrains.annotations.NotNull()
    java.util.List<? extends io.app.nycschools.Models.NycSchoolsModel> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.rideplus.partner.ui.adapters.NycSchoolListAdapter.ItemSelectionListener getItemSelectionListener() {
        return null;
    }
    
    public final void setItemSelectionListener(@org.jetbrains.annotations.NotNull()
    com.rideplus.partner.ui.adapters.NycSchoolListAdapter.ItemSelectionListener p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.Context getContext() {
        return null;
    }
    
    public final void setContext(@org.jetbrains.annotations.NotNull()
    android.content.Context p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final io.app.nycschools.databinding.ItemCustomerListBinding getBinding() {
        return null;
    }
    
    public final void setBinding(@org.jetbrains.annotations.Nullable()
    io.app.nycschools.databinding.ItemCustomerListBinding p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public com.rideplus.partner.ui.adapters.NycSchoolListAdapter.CheckHolder onCreateViewHolder(@org.jetbrains.annotations.NotNull()
    android.view.ViewGroup parent, int viewType) {
        return null;
    }
    
    @java.lang.Override()
    public int getItemCount() {
        return 0;
    }
    
    @java.lang.Override()
    public void onBindViewHolder(@org.jetbrains.annotations.NotNull()
    com.rideplus.partner.ui.adapters.NycSchoolListAdapter.CheckHolder holder, int position) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001c\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0018\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u0007H&\u00a8\u0006\b"}, d2 = {"Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter$ItemSelectionListener;", "", "onItemClick", "", "position", "", "rideList", "Lio/app/nycschools/Models/NycSchoolsModel;", "app_debug"})
    public static abstract interface ItemSelectionListener {
        
        public abstract void onItemClick(int position, @org.jetbrains.annotations.NotNull()
        io.app.nycschools.Models.NycSchoolsModel rideList);
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0010\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0007R\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\b\u00a8\u0006\r"}, d2 = {"Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter$CheckHolder;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "itemBinding", "Lio/app/nycschools/databinding/ItemCustomerListBinding;", "(Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter;Lio/app/nycschools/databinding/ItemCustomerListBinding;)V", "getItemBinding", "()Lio/app/nycschools/databinding/ItemCustomerListBinding;", "setItemBinding", "(Lio/app/nycschools/databinding/ItemCustomerListBinding;)V", "setData", "", "model", "Lio/app/nycschools/Models/NycSchoolsModel;", "app_debug"})
    public final class CheckHolder extends androidx.recyclerview.widget.RecyclerView.ViewHolder {
        @org.jetbrains.annotations.NotNull()
        private io.app.nycschools.databinding.ItemCustomerListBinding itemBinding;
        
        public CheckHolder(@org.jetbrains.annotations.NotNull()
        io.app.nycschools.databinding.ItemCustomerListBinding itemBinding) {
            super(null);
        }
        
        @org.jetbrains.annotations.NotNull()
        public final io.app.nycschools.databinding.ItemCustomerListBinding getItemBinding() {
            return null;
        }
        
        public final void setItemBinding(@org.jetbrains.annotations.NotNull()
        io.app.nycschools.databinding.ItemCustomerListBinding p0) {
        }
        
        @android.annotation.SuppressLint(value = {"SetTextI18n"})
        public final void setData(@org.jetbrains.annotations.NotNull()
        io.app.nycschools.Models.NycSchoolsModel model) {
        }
    }
}