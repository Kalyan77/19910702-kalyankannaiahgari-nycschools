package io.app.nycschools.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000 \n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\b&\u0018\u00002\b\u0012\u0004\u0012\u00020\u00020\u0001:\u0001\fB\u0005\u00a2\u0006\u0002\u0010\u0003J\u000e\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0004\u001a\u00020\u0005R\u001a\u0010\u0004\u001a\u00020\u0005X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\r"}, d2 = {"Lio/app/nycschools/base/BaseRecyclerView;", "Landroidx/recyclerview/widget/RecyclerView$Adapter;", "Landroidx/recyclerview/widget/RecyclerView$ViewHolder;", "()V", "itemClickListener", "Lio/app/nycschools/base/BaseRecyclerView$ItemClickListener;", "getItemClickListener", "()Lio/app/nycschools/base/BaseRecyclerView$ItemClickListener;", "setItemClickListener", "(Lio/app/nycschools/base/BaseRecyclerView$ItemClickListener;)V", "setListener", "", "ItemClickListener", "app_debug"})
public abstract class BaseRecyclerView extends androidx.recyclerview.widget.RecyclerView.Adapter<androidx.recyclerview.widget.RecyclerView.ViewHolder> {
    public io.app.nycschools.base.BaseRecyclerView.ItemClickListener itemClickListener;
    
    public BaseRecyclerView() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.app.nycschools.base.BaseRecyclerView.ItemClickListener getItemClickListener() {
        return null;
    }
    
    public final void setItemClickListener(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.base.BaseRecyclerView.ItemClickListener p0) {
    }
    
    public final void setListener(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.base.BaseRecyclerView.ItemClickListener itemClickListener) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0016\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0010\u0010\u0002\u001a\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005H&\u00a8\u0006\u0006"}, d2 = {"Lio/app/nycschools/base/BaseRecyclerView$ItemClickListener;", "", "onItemClicked", "", "model", "Lio/app/nycschools/base/BaseModel;", "app_debug"})
    public static abstract interface ItemClickListener {
        
        public abstract void onItemClicked(@org.jetbrains.annotations.NotNull()
        io.app.nycschools.base.BaseModel model);
    }
}