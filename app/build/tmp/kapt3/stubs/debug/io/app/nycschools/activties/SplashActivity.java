package io.app.nycschools.activties;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\u0018\u0000 \"2\u00020\u0001:\u0001\"B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0014\u001a\u00020\u0015H\u0002J\u0012\u0010\u0016\u001a\u00020\u00152\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0014J\b\u0010\u0019\u001a\u00020\u0015H\u0014J\b\u0010\u001a\u001a\u00020\u0015H\u0014J \u0010\u001b\u001a\u00020\u00152\u0006\u0010\u001c\u001a\u00020\u001d2\u0006\u0010\u001e\u001a\u00020\u001f2\u0006\u0010 \u001a\u00020\fH\u0016J\b\u0010!\u001a\u00020\u0015H\u0002R\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0007\u0010\b\"\u0004\b\t\u0010\nR\u001a\u0010\u000b\u001a\u00020\fX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\r\u0010\u000e\"\u0004\b\u000f\u0010\u0010R\u001c\u0010\u0011\u001a\u0004\u0018\u00010\u0006X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0012\u0010\b\"\u0004\b\u0013\u0010\n\u00a8\u0006#"}, d2 = {"Lio/app/nycschools/activties/SplashActivity;", "Lio/app/nycschools/base/BaseActivity;", "()V", "context", "Landroid/content/Context;", "loggedInAs", "", "getLoggedInAs", "()Ljava/lang/String;", "setLoggedInAs", "(Ljava/lang/String;)V", "logged_r_not", "", "getLogged_r_not", "()Z", "setLogged_r_not", "(Z)V", "mobile", "getMobile", "setMobile", "launchHomeScreen", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "onStart", "setWindowFlag", "activity", "Landroid/app/Activity;", "bits", "", "on", "transparentToolbar", "Companion", "app_debug"})
public final class SplashActivity extends io.app.nycschools.base.BaseActivity {
    private android.content.Context context;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String mobile;
    @org.jetbrains.annotations.NotNull()
    private java.lang.String loggedInAs = "";
    private boolean logged_r_not = false;
    @org.jetbrains.annotations.NotNull()
    public static final io.app.nycschools.activties.SplashActivity.Companion Companion = null;
    private static final int SPLASH_TIME_OUT = 1000;
    private static final java.lang.String TAG = null;
    private java.util.HashMap _$_findViewCache;
    
    public SplashActivity() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getMobile() {
        return null;
    }
    
    public final void setMobile(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLoggedInAs() {
        return null;
    }
    
    public final void setLoggedInAs(@org.jetbrains.annotations.NotNull()
    java.lang.String p0) {
    }
    
    public final boolean getLogged_r_not() {
        return false;
    }
    
    public final void setLogged_r_not(boolean p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    private final void launchHomeScreen() {
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    private final void transparentToolbar() {
    }
    
    public void setWindowFlag(@org.jetbrains.annotations.NotNull()
    android.app.Activity activity, int bits, boolean on) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0016\u0010\u0005\u001a\n \u0007*\u0004\u0018\u00010\u00060\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lio/app/nycschools/activties/SplashActivity$Companion;", "", "()V", "SPLASH_TIME_OUT", "", "TAG", "", "kotlin.jvm.PlatformType", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
    }
}