package io.app.nycschools.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00000\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u000e\u0010\u0003\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u0016\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\f\u001a\u00020\rJ\u0016\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000e2\u0006\u0010\f\u001a\u00020\r\u00a8\u0006\u000f"}, d2 = {"Lio/app/nycschools/util/PermissionUtils;", "", "()V", "isAccessFineLocationGranted", "", "context", "Landroid/content/Context;", "isLocationEnabled", "requestAccessFineLocationPermission", "", "activity", "Landroidx/appcompat/app/AppCompatActivity;", "requestId", "", "Landroidx/fragment/app/FragmentActivity;", "app_debug"})
public final class PermissionUtils {
    @org.jetbrains.annotations.NotNull()
    public static final io.app.nycschools.util.PermissionUtils INSTANCE = null;
    
    private PermissionUtils() {
        super();
    }
    
    /**
     * Function to request permission from the user
     */
    public final void requestAccessFineLocationPermission(@org.jetbrains.annotations.NotNull()
    androidx.appcompat.app.AppCompatActivity activity, int requestId) {
    }
    
    public final void requestAccessFineLocationPermission(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.FragmentActivity activity, int requestId) {
    }
    
    /**
     * Function to check if the location permissions are granted or not
     */
    public final boolean isAccessFineLocationGranted(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    /**
     * Function to check if location of the device is enabled or not
     */
    public final boolean isLocationEnabled(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
}