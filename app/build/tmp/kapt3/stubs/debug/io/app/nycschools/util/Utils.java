package io.app.nycschools.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\u0018\u0000 \u00032\u00020\u0001:\u0001\u0003B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0004"}, d2 = {"Lio/app/nycschools/util/Utils;", "", "()V", "Companion", "app_debug"})
public final class Utils {
    @org.jetbrains.annotations.NotNull()
    public static final io.app.nycschools.util.Utils.Companion Companion = null;
    private static long lastClickTime = 0L;
    private static long mLastClickTime = 0L;
    
    public Utils() {
        super();
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00004\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0006\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\n\u001a\u0004\u0018\u00010\u000b2\u0006\u0010\f\u001a\u00020\u000bJ\u0012\u0010\r\u001a\u0004\u0018\u00010\u000b2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000bJ\b\u0010\u000f\u001a\u0004\u0018\u00010\u000bJ\u0006\u0010\u0010\u001a\u00020\u0011J\u0010\u0010\u0012\u001a\u00020\u00112\u0006\u0010\u0013\u001a\u00020\u0014H\u0007J\u0006\u0010\u0015\u001a\u00020\u0011J\u0016\u0010\u0016\u001a\u00020\u00172\u0006\u0010\u0018\u001a\u00020\u000b2\u0006\u0010\u0019\u001a\u00020\u000bR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0005\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\u001a"}, d2 = {"Lio/app/nycschools/util/Utils$Companion;", "", "()V", "lastClickTime", "", "mLastClickTime", "getMLastClickTime", "()J", "setMLastClickTime", "(J)V", "capitalizeFirstLetter", "", "s", "convertToCurrentTimeZone", "Date", "getCurrentTimeZone", "isFastDoubleClick", "", "isNetworkAvailable", "context", "Landroid/content/Context;", "isOpenRecently", "logInfo", "", "tag", "value", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @androidx.annotation.RequiresApi(value = android.os.Build.VERSION_CODES.N)
        public final boolean isNetworkAvailable(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return false;
        }
        
        public final void logInfo(@org.jetbrains.annotations.NotNull()
        java.lang.String tag, @org.jetbrains.annotations.NotNull()
        java.lang.String value) {
        }
        
        public final boolean isFastDoubleClick() {
            return false;
        }
        
        public final long getMLastClickTime() {
            return 0L;
        }
        
        public final void setMLastClickTime(long p0) {
        }
        
        public final boolean isOpenRecently() {
            return false;
        }
        
        /**
         * This method is used to capitalize first Letter
         */
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String capitalizeFirstLetter(@org.jetbrains.annotations.NotNull()
        java.lang.String s) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String convertToCurrentTimeZone(@org.jetbrains.annotations.Nullable()
        java.lang.String Date) {
            return null;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getCurrentTimeZone() {
            return null;
        }
    }
}