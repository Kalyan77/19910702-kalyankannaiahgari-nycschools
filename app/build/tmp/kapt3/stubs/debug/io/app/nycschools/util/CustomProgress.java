package io.app.nycschools.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\n\u001a\u0004\u0018\u00010\u0000J\u0006\u0010\u000b\u001a\u00020\fR\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u0000X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0004\u001a\u0004\u0018\u00010\u0005X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0006\u0010\u0007\"\u0004\b\b\u0010\t\u00a8\u0006\r"}, d2 = {"Lio/app/nycschools/util/CustomProgress;", "", "()V", "customProgress", "mDialog", "Landroid/app/Dialog;", "getMDialog", "()Landroid/app/Dialog;", "setMDialog", "(Landroid/app/Dialog;)V", "getInstance", "hideProgress", "", "app_debug"})
public final class CustomProgress {
    private io.app.nycschools.util.CustomProgress customProgress;
    @org.jetbrains.annotations.Nullable()
    private android.app.Dialog mDialog;
    
    public CustomProgress() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.Dialog getMDialog() {
        return null;
    }
    
    public final void setMDialog(@org.jetbrains.annotations.Nullable()
    android.app.Dialog p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final io.app.nycschools.util.CustomProgress getInstance() {
        return null;
    }
    
    public final void hideProgress() {
    }
}