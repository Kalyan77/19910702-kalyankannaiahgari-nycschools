package io.app.nycschools.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lio/app/nycschools/di/module/AppModule;", "", "()V", "app_debug"})
@dagger.Module(includes = {io.app.nycschools.di.module.ViewModelModule.class, io.app.nycschools.di.module.RepositoryModule.class, io.app.nycschools.di.module.NetworkModule.class, io.app.nycschools.di.module.ActivityContributorsModule.class})
public final class AppModule {
    
    public AppModule() {
        super();
    }
}