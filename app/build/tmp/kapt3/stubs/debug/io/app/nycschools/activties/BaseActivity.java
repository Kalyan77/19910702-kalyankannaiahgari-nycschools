package io.app.nycschools.activties;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000L\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0010\t\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u000f\b\u0016\u0018\u0000 .2\u00020\u0001:\u0003./0B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u001c\u001a\u00020\u001dJ\u001e\u0010\u001e\u001a\u00020\n2\u0006\u0010\u001f\u001a\u00020 2\u0006\u0010!\u001a\u00020\"2\u0006\u0010#\u001a\u00020\u0010J\b\u0010$\u001a\u00020\u001dH\u0014J\b\u0010%\u001a\u00020\u001dH\u0014J\u0006\u0010&\u001a\u00020\u001dJ\u0010\u0010\'\u001a\u00020\u001d2\b\u0010(\u001a\u0004\u0018\u00010\u0015J\u0006\u0010)\u001a\u00020\u001dJ\u0010\u0010*\u001a\u00020\u001d2\b\u0010(\u001a\u0004\u0018\u00010\u0015J\u0006\u0010+\u001a\u00020\u001dJ\u001a\u0010,\u001a\u00020\u001d2\b\u0010\u001f\u001a\u0004\u0018\u00010 2\b\u0010-\u001a\u0004\u0018\u00010\u0015R\u001c\u0010\u0003\u001a\u0004\u0018\u00010\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001c\u0010\t\u001a\u0004\u0018\u00010\nX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001e\u0010\u0011\u001a\u00020\u00102\u0006\u0010\u000f\u001a\u00020\u0010@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0012R \u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00150\u0014X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0016\u0010\u0017\"\u0004\b\u0018\u0010\u0019R\u000e\u0010\u001a\u001a\u00020\u001bX\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u00061"}, d2 = {"Lio/app/nycschools/activties/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "()V", "alertDialog", "Landroid/app/AlertDialog;", "getAlertDialog", "()Landroid/app/AlertDialog;", "setAlertDialog", "(Landroid/app/AlertDialog;)V", "dialog", "Landroid/app/Dialog;", "getDialog", "()Landroid/app/Dialog;", "setDialog", "(Landroid/app/Dialog;)V", "<set-?>", "", "isShown", "()Z", "listAll", "Ljava/util/ArrayList;", "", "getListAll", "()Ljava/util/ArrayList;", "setListAll", "(Ljava/util/ArrayList;)V", "mLastClickTime", "", "closeProgress", "", "commonDialog", "context", "Landroid/content/Context;", "layoutDialog", "", "isCancelable", "onPause", "onResume", "showAlertDialog", "showAlertDialogForNoData", "s", "showAlertDialogForSplash", "showAlertDialogForSuccess", "showProgress", "showToast", "message", "Companion", "ItemOffsetDecoration", "RecyclerTouchListener", "app_debug"})
public class BaseActivity extends androidx.appcompat.app.AppCompatActivity {
    @org.jetbrains.annotations.Nullable()
    private android.app.AlertDialog alertDialog;
    @org.jetbrains.annotations.Nullable()
    private android.app.Dialog dialog;
    private boolean isShown = false;
    private long mLastClickTime = 0L;
    @org.jetbrains.annotations.NotNull()
    private java.util.ArrayList<java.lang.String> listAll;
    @org.jetbrains.annotations.NotNull()
    public static final io.app.nycschools.activties.BaseActivity.Companion Companion = null;
    private java.util.HashMap _$_findViewCache;
    
    public BaseActivity() {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.AlertDialog getAlertDialog() {
        return null;
    }
    
    public final void setAlertDialog(@org.jetbrains.annotations.Nullable()
    android.app.AlertDialog p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.Dialog getDialog() {
        return null;
    }
    
    public final void setDialog(@org.jetbrains.annotations.Nullable()
    android.app.Dialog p0) {
    }
    
    public final boolean isShown() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.ArrayList<java.lang.String> getListAll() {
        return null;
    }
    
    public final void setListAll(@org.jetbrains.annotations.NotNull()
    java.util.ArrayList<java.lang.String> p0) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    public final void showToast(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    public final void showAlertDialog() {
    }
    
    public final void showAlertDialogForSplash() {
    }
    
    public final void showAlertDialogForSuccess(@org.jetbrains.annotations.Nullable()
    java.lang.String s) {
    }
    
    public final void showAlertDialogForNoData(@org.jetbrains.annotations.Nullable()
    java.lang.String s) {
    }
    
    public final void showProgress() {
    }
    
    public final void closeProgress() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Dialog commonDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int layoutDialog, boolean isCancelable) {
        return null;
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0004\n\u0002\u0010\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0004\u0018\u00002\u00020\u0001B\u0019\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\b\b\u0001\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\u0006B\r\u0012\u0006\u0010\u0007\u001a\u00020\u0005\u00a2\u0006\u0002\u0010\bJ(\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u000e2\u0006\u0010\u000f\u001a\u00020\u00102\u0006\u0010\u0011\u001a\u00020\u0012H\u0016R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0013"}, d2 = {"Lio/app/nycschools/activties/BaseActivity$ItemOffsetDecoration;", "Landroidx/recyclerview/widget/RecyclerView$ItemDecoration;", "context", "Landroid/content/Context;", "itemOffsetId", "", "(Lio/app/nycschools/activties/BaseActivity;Landroid/content/Context;I)V", "mItemOffset", "(Lio/app/nycschools/activties/BaseActivity;I)V", "getItemOffsets", "", "outRect", "Landroid/graphics/Rect;", "view", "Landroid/view/View;", "parent", "Landroidx/recyclerview/widget/RecyclerView;", "state", "Landroidx/recyclerview/widget/RecyclerView$State;", "app_debug"})
    public final class ItemOffsetDecoration extends androidx.recyclerview.widget.RecyclerView.ItemDecoration {
        private final int mItemOffset = 0;
        
        public ItemOffsetDecoration(int mItemOffset) {
            super();
        }
        
        public ItemOffsetDecoration(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @androidx.annotation.DimenRes()
        int itemOffsetId) {
            super();
        }
        
        @java.lang.Override()
        public void getItemOffsets(@org.jetbrains.annotations.NotNull()
        android.graphics.Rect outRect, @org.jetbrains.annotations.NotNull()
        android.view.View view, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView parent, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView.State state) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000:\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0003\b\u0080\u0004\u0018\u00002\u00020\u0001B!\u0012\b\u0010\u0002\u001a\u0004\u0018\u00010\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\b\u0010\u0006\u001a\u0004\u0018\u00010\u0007\u00a2\u0006\u0002\u0010\bJ\u0018\u0010\u000b\u001a\u00020\f2\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u000fH\u0016J\u0010\u0010\u0010\u001a\u00020\u00112\u0006\u0010\u0012\u001a\u00020\fH\u0016J\u0018\u0010\u0013\u001a\u00020\u00112\u0006\u0010\r\u001a\u00020\u00052\u0006\u0010\u000e\u001a\u00020\u000fH\u0016R\u0010\u0010\u0006\u001a\u0004\u0018\u00010\u0007X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\nX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0014"}, d2 = {"Lio/app/nycschools/activties/BaseActivity$RecyclerTouchListener;", "Landroidx/recyclerview/widget/RecyclerView$OnItemTouchListener;", "context", "Landroid/content/Context;", "recycleView", "Landroidx/recyclerview/widget/RecyclerView;", "clicklistener", "Lio/app/nycschools/repositories/ClickListener;", "(Lio/app/nycschools/activties/BaseActivity;Landroid/content/Context;Landroidx/recyclerview/widget/RecyclerView;Lio/app/nycschools/repositories/ClickListener;)V", "gestureDetector", "Landroid/view/GestureDetector;", "onInterceptTouchEvent", "", "rv", "e", "Landroid/view/MotionEvent;", "onRequestDisallowInterceptTouchEvent", "", "disallowIntercept", "onTouchEvent", "app_debug"})
    public final class RecyclerTouchListener implements androidx.recyclerview.widget.RecyclerView.OnItemTouchListener {
        private final io.app.nycschools.repositories.ClickListener clicklistener = null;
        private final android.view.GestureDetector gestureDetector = null;
        
        public RecyclerTouchListener(@org.jetbrains.annotations.Nullable()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView recycleView, @org.jetbrains.annotations.Nullable()
        io.app.nycschools.repositories.ClickListener clicklistener) {
            super();
        }
        
        @java.lang.Override()
        public boolean onInterceptTouchEvent(@org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView rv, @org.jetbrains.annotations.NotNull()
        android.view.MotionEvent e) {
            return false;
        }
        
        @java.lang.Override()
        public void onTouchEvent(@org.jetbrains.annotations.NotNull()
        androidx.recyclerview.widget.RecyclerView rv, @org.jetbrains.annotations.NotNull()
        android.view.MotionEvent e) {
        }
        
        @java.lang.Override()
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        }
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u001a\u0010\u0007\u001a\u00020\b2\u0006\u0010\u0005\u001a\u00020\u00062\n\u0010\t\u001a\u0006\u0012\u0002\b\u00030\n\u00a8\u0006\u000b"}, d2 = {"Lio/app/nycschools/activties/BaseActivity$Companion;", "", "()V", "getUniqueID", "", "context", "Landroid/content/Context;", "isServiceRunning", "", "serviceClass", "Ljava/lang/Class;", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        public final boolean isServiceRunning(@org.jetbrains.annotations.NotNull()
        android.content.Context context, @org.jetbrains.annotations.NotNull()
        java.lang.Class<?> serviceClass) {
            return false;
        }
        
        @org.jetbrains.annotations.Nullable()
        public final java.lang.String getUniqueID(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
    }
}