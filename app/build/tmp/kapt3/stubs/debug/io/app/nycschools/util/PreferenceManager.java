package io.app.nycschools.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\b\n\u0002\b\n\u0018\u0000 \u00162\u00020\u0001:\u0001\u0016B\u000f\b\u0016\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u0006\u0010\u0006\u001a\u00020\u0007J\u000e\u0010\b\u001a\u00020\t2\u0006\u0010\n\u001a\u00020\u000bJ\u000e\u0010\f\u001a\u00020\r2\u0006\u0010\n\u001a\u00020\u000bJ\u0010\u0010\u000e\u001a\u00020\u000b2\b\u0010\n\u001a\u0004\u0018\u00010\u000bJ\u0006\u0010\u000f\u001a\u00020\u000bJ\u000e\u0010\u0010\u001a\u00020\u00072\u0006\u0010\u0011\u001a\u00020\u000bJ\u0016\u0010\u0012\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\tJ\u0016\u0010\u0014\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000b2\u0006\u0010\u0013\u001a\u00020\rJ\u0018\u0010\u0015\u001a\u00020\u00072\u0006\u0010\n\u001a\u00020\u000b2\b\u0010\u0013\u001a\u0004\u0018\u00010\u000b\u00a8\u0006\u0017"}, d2 = {"Lio/app/nycschools/util/PreferenceManager;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "()V", "clearAppData", "", "getSavedBoolean", "", "key", "", "getSavedInteger", "", "getSavedString", "getToken", "saveAccessToken", "token", "saveBoolean", "value", "saveInteger", "saveString", "Companion", "app_debug"})
public final class PreferenceManager {
    @org.jetbrains.annotations.NotNull()
    public static final io.app.nycschools.util.PreferenceManager.Companion Companion = null;
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String ACCESS_TOKEN = "ACCESS_TOKEN";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String fragment = "fragment";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String DEVICE_FCM_TOKEN = "DEVICE_FCM_TOKEN";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String Secondprogile = "Secondprogile";
    private static final java.lang.String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    @org.jetbrains.annotations.NotNull()
    private static java.lang.String unique_string = "unique_string";
    private static final java.lang.String IS_LOGIN1 = "IsLoggedIn1";
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String IS_LOGIN = "IS_LOGIN";
    private static android.content.SharedPreferences sharedPreferences;
    private static android.content.SharedPreferences.Editor editor;
    
    public PreferenceManager() {
        super();
    }
    
    public PreferenceManager(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    public final void saveString(@org.jetbrains.annotations.NotNull()
    java.lang.String key, @org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSavedString(@org.jetbrains.annotations.Nullable()
    java.lang.String key) {
        return null;
    }
    
    public final void saveBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String key, boolean value) {
    }
    
    public final boolean getSavedBoolean(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return false;
    }
    
    public final void saveInteger(@org.jetbrains.annotations.NotNull()
    java.lang.String key, int value) {
    }
    
    public final int getSavedInteger(@org.jetbrains.annotations.NotNull()
    java.lang.String key) {
        return 0;
    }
    
    public final void clearAppData() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getToken() {
        return null;
    }
    
    public final void saveAccessToken(@org.jetbrains.annotations.NotNull()
    java.lang.String token) {
    }
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0004\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002R\u001a\u0010\u0003\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u000e\u0010\t\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0004X\u0086T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u001a\u0010\r\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000e\u0010\u0006\"\u0004\b\u000f\u0010\bR\u000e\u0010\u0010\u001a\u00020\u0011X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0012\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0013\u0010\u0006\"\u0004\b\u0014\u0010\bR\u000e\u0010\u0015\u001a\u00020\u0016X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u0010\u0017\u001a\u00020\u0004X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0018\u0010\u0006\"\u0004\b\u0019\u0010\b\u00a8\u0006\u001a"}, d2 = {"Lio/app/nycschools/util/PreferenceManager$Companion;", "", "()V", "ACCESS_TOKEN", "", "getACCESS_TOKEN", "()Ljava/lang/String;", "setACCESS_TOKEN", "(Ljava/lang/String;)V", "DEVICE_FCM_TOKEN", "IS_FIRST_TIME_LAUNCH", "IS_LOGIN", "IS_LOGIN1", "Secondprogile", "getSecondprogile", "setSecondprogile", "editor", "Landroid/content/SharedPreferences$Editor;", "fragment", "getFragment", "setFragment", "sharedPreferences", "Landroid/content/SharedPreferences;", "unique_string", "getUnique_string", "setUnique_string", "app_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getACCESS_TOKEN() {
            return null;
        }
        
        public final void setACCESS_TOKEN(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getFragment() {
            return null;
        }
        
        public final void setFragment(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getSecondprogile() {
            return null;
        }
        
        public final void setSecondprogile(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
        
        @org.jetbrains.annotations.NotNull()
        public final java.lang.String getUnique_string() {
            return null;
        }
        
        public final void setUnique_string(@org.jetbrains.annotations.NotNull()
        java.lang.String p0) {
        }
    }
}