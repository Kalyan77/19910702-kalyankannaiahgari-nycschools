package io.app.nycschools.activties;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000@\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0016\u0010\u000f\u001a\u00020\u00102\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00130\u0012H\u0002J\u0006\u0010\u0014\u001a\u00020\u0010J\b\u0010\u0015\u001a\u00020\u0010H\u0016J\u0012\u0010\u0016\u001a\u00020\u00102\b\u0010\u0017\u001a\u0004\u0018\u00010\u0018H\u0014J\b\u0010\u0019\u001a\u00020\u0010H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0082\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\u00020\nX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000e\u00a8\u0006\u001a"}, d2 = {"Lio/app/nycschools/activties/NycSchoolsListActivity;", "Lio/app/nycschools/base/BaseActivity;", "()V", "binding", "Lio/app/nycschools/databinding/ActivityNycListBinding;", "linearLayoutManager1", "Landroidx/recyclerview/widget/LinearLayoutManager;", "nycSchoolListAdapter", "Lcom/rideplus/partner/ui/adapters/NycSchoolListAdapter;", "viewModel", "Lio/app/nycschools/viewmodel/HomeViewModel;", "getViewModel", "()Lio/app/nycschools/viewmodel/HomeViewModel;", "setViewModel", "(Lio/app/nycschools/viewmodel/HomeViewModel;)V", "bindAdapterCustomerList", "", "nycSchoolsModel", "", "Lio/app/nycschools/Models/NycSchoolsModel;", "getSchoolsList", "onBackPressed", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "onResume", "app_debug"})
public final class NycSchoolsListActivity extends io.app.nycschools.base.BaseActivity {
    private io.app.nycschools.databinding.ActivityNycListBinding binding;
    public io.app.nycschools.viewmodel.HomeViewModel viewModel;
    private androidx.recyclerview.widget.LinearLayoutManager linearLayoutManager1;
    private com.rideplus.partner.ui.adapters.NycSchoolListAdapter nycSchoolListAdapter;
    private java.util.HashMap _$_findViewCache;
    
    public NycSchoolsListActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.app.nycschools.viewmodel.HomeViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.viewmodel.HomeViewModel p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    public void onBackPressed() {
    }
    
    public final void getSchoolsList() {
    }
    
    private final void bindAdapterCustomerList(java.util.List<? extends io.app.nycschools.Models.NycSchoolsModel> nycSchoolsModel) {
    }
}