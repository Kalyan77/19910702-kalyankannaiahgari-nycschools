package io.app.nycschools.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000&\n\u0000\n\u0002\u0010\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\u0010\u000e\n\u0002\b\f\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0002\u001a\u0012\u0010\u0000\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0004\u001a\u00020\u0005\u001a\n\u0010\u0000\u001a\u00020\u0001*\u00020\u0006\u001a\n\u0010\u0007\u001a\u00020\b*\u00020\t\u001a\n\u0010\n\u001a\u00020\b*\u00020\t\u001a\n\u0010\u000b\u001a\u00020\b*\u00020\t\u001a\n\u0010\f\u001a\u00020\b*\u00020\t\u001a\n\u0010\r\u001a\u00020\b*\u00020\t\u001a\n\u0010\u000e\u001a\u00020\b*\u00020\t\u001a\n\u0010\u000f\u001a\u00020\b*\u00020\t\u001a\u0012\u0010\u0010\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0011\u001a\u00020\t\u001a\u0015\u0010\u0012\u001a\u00020\u0001*\u00020\u00052\u0006\u0010\u0013\u001a\u00020\tH\u0086\b\u001a\u0012\u0010\u0014\u001a\u00020\u0001*\u00020\u00032\u0006\u0010\u0013\u001a\u00020\t\u00a8\u0006\u0015"}, d2 = {"hideKeyboard", "", "Landroid/app/Activity;", "Landroid/content/Context;", "view", "Landroid/view/View;", "Landroidx/fragment/app/Fragment;", "isEmailValid", "", "", "isValidAdhaarNumber", "isValidEmail", "isValidIFC", "isValidMobileNumber", "isValidPanNumber", "isVehicleNumberValid", "log", "string", "showSnackBar", "message", "showToast", "app_debug"})
public final class ExtentionFuctionsKt {
    
    public static final void showToast(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$showToast, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public static final void log(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$log, @org.jetbrains.annotations.NotNull()
    java.lang.String string) {
    }
    
    public static final boolean isValidEmail(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isValidEmail) {
        return false;
    }
    
    public static final boolean isValidIFC(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isValidIFC) {
        return false;
    }
    
    public static final boolean isEmailValid(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isEmailValid) {
        return false;
    }
    
    public static final void showSnackBar(@org.jetbrains.annotations.NotNull()
    android.view.View $this$showSnackBar, @org.jetbrains.annotations.NotNull()
    java.lang.String message) {
    }
    
    public static final boolean isVehicleNumberValid(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isVehicleNumberValid) {
        return false;
    }
    
    public static final boolean isValidMobileNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isValidMobileNumber) {
        return false;
    }
    
    public static final boolean isValidPanNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isValidPanNumber) {
        return false;
    }
    
    public static final boolean isValidAdhaarNumber(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isValidAdhaarNumber) {
        return false;
    }
    
    public static final void hideKeyboard(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$hideKeyboard) {
    }
    
    public static final void hideKeyboard(@org.jetbrains.annotations.NotNull()
    android.app.Activity $this$hideKeyboard) {
    }
    
    public static final void hideKeyboard(@org.jetbrains.annotations.NotNull()
    android.content.Context $this$hideKeyboard, @org.jetbrains.annotations.NotNull()
    android.view.View view) {
    }
}