package io.app.nycschools.util.networkutils;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010\u0003\n\u0000\n\u0002\u0010\u000b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0003\u001a\u0004\u0018\u00010\u00042\u0006\u0010\u0005\u001a\u00020\u0006J\u000e\u0010\u0007\u001a\u00020\b2\u0006\u0010\t\u001a\u00020\nJ\u0006\u0010\u000b\u001a\u00020\f\u00a8\u0006\r"}, d2 = {"Lio/app/nycschools/util/networkutils/CommonFunctions;", "", "()V", "getErrorData", "", "it", "", "isOnline", "", "context", "Landroid/content/Context;", "networkRegistration", "Landroid/content/IntentFilter;", "app_debug"})
public final class CommonFunctions {
    @org.jetbrains.annotations.NotNull()
    public static final io.app.nycschools.util.networkutils.CommonFunctions INSTANCE = null;
    
    private CommonFunctions() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.IntentFilter networkRegistration() {
        return null;
    }
    
    public final boolean isOnline(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        return false;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getErrorData(@org.jetbrains.annotations.NotNull()
    java.lang.Throwable it) {
        return null;
    }
}