package io.app.nycschools.activties;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u00006\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0013\u001a\u00020\u0014J\u0012\u0010\u0015\u001a\u00020\u00142\b\u0010\u0016\u001a\u0004\u0018\u00010\u0017H\u0014R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082.\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082.\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0007\u001a\u0004\u0018\u00010\bX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\t\u0010\n\"\u0004\b\u000b\u0010\fR\u001a\u0010\r\u001a\u00020\u000eX\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012\u00a8\u0006\u0018"}, d2 = {"Lio/app/nycschools/activties/NycSchoolDataActivity;", "Lio/app/nycschools/base/BaseActivity;", "()V", "appBarConfiguration", "Landroidx/navigation/ui/AppBarConfiguration;", "binding", "Lio/app/nycschools/databinding/ActivityNycSchoolDataBinding;", "dbn", "", "getDbn", "()Ljava/lang/String;", "setDbn", "(Ljava/lang/String;)V", "viewModel", "Lio/app/nycschools/viewmodel/HomeViewModel;", "getViewModel", "()Lio/app/nycschools/viewmodel/HomeViewModel;", "setViewModel", "(Lio/app/nycschools/viewmodel/HomeViewModel;)V", "getSatData", "", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "app_debug"})
public final class NycSchoolDataActivity extends io.app.nycschools.base.BaseActivity {
    private androidx.navigation.ui.AppBarConfiguration appBarConfiguration;
    private io.app.nycschools.databinding.ActivityNycSchoolDataBinding binding;
    public io.app.nycschools.viewmodel.HomeViewModel viewModel;
    @org.jetbrains.annotations.Nullable()
    private java.lang.String dbn;
    private java.util.HashMap _$_findViewCache;
    
    public NycSchoolDataActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.app.nycschools.viewmodel.HomeViewModel getViewModel() {
        return null;
    }
    
    public final void setViewModel(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.viewmodel.HomeViewModel p0) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getDbn() {
        return null;
    }
    
    public final void setDbn(@org.jetbrains.annotations.Nullable()
    java.lang.String p0) {
    }
    
    @java.lang.Override()
    protected void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState) {
    }
    
    public final void getSatData() {
    }
}