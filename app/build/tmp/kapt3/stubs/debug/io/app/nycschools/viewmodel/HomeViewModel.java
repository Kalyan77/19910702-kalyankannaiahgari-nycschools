package io.app.nycschools.viewmodel;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000&\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\u0018\u00002\u00020\u0001B\u000f\b\u0007\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0012\u0010\b\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u000b0\n0\tJ\u0012\u0010\f\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\r0\n0\tR\u001a\u0010\u0002\u001a\u00020\u0003X\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\u0004\u00a8\u0006\u000e"}, d2 = {"Lio/app/nycschools/viewmodel/HomeViewModel;", "Landroidx/lifecycle/ViewModel;", "homeRepository", "Lio/app/nycschools/repositories/HomeRepository;", "(Lio/app/nycschools/repositories/HomeRepository;)V", "getHomeRepository", "()Lio/app/nycschools/repositories/HomeRepository;", "setHomeRepository", "getSatData", "Landroidx/lifecycle/MutableLiveData;", "", "Lio/app/nycschools/Models/NycSatModel;", "getSchoolsList", "Lio/app/nycschools/Models/NycSchoolsModel;", "app_debug"})
public final class HomeViewModel extends androidx.lifecycle.ViewModel {
    @org.jetbrains.annotations.NotNull()
    private io.app.nycschools.repositories.HomeRepository homeRepository;
    
    @javax.inject.Inject()
    public HomeViewModel(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.repositories.HomeRepository homeRepository) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.app.nycschools.repositories.HomeRepository getHomeRepository() {
        return null;
    }
    
    public final void setHomeRepository(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.repositories.HomeRepository p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<io.app.nycschools.Models.NycSchoolsModel>> getSchoolsList() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.MutableLiveData<java.util.List<io.app.nycschools.Models.NycSatModel>> getSatData() {
        return null;
    }
}