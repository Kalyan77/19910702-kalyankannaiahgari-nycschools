package io.app.nycschools;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000<\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0002\n\u0000\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010\r\u001a\b\u0012\u0004\u0012\u00020\b0\u000eH\u0016J\u000e\u0010\u000f\u001a\b\u0012\u0004\u0012\u00020\n0\u000eH\u0016J\b\u0010\u0010\u001a\u00020\u0011H\u0016R\u001a\u0010\u0006\u001a\n\u0012\u0004\u0012\u00020\b\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u001a\u0010\t\u001a\n\u0012\u0004\u0012\u00020\n\u0018\u00010\u00078\u0006@\u0006X\u0087\u000e\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\fX\u0082.\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u0012"}, d2 = {"Lio/app/nycschools/AppApplication;", "Landroid/app/Application;", "Ldagger/android/HasActivityInjector;", "Ldagger/android/HasFragmentInjector;", "Landroidx/lifecycle/LifecycleObserver;", "()V", "activityDispatchingAndroidInjector", "Ldagger/android/DispatchingAndroidInjector;", "Landroid/app/Activity;", "fragmentDispatchingAndroidInjector", "Landroid/app/Fragment;", "job", "Lkotlinx/coroutines/Job;", "activityInjector", "Ldagger/android/AndroidInjector;", "fragmentInjector", "onCreate", "", "app_debug"})
public final class AppApplication extends android.app.Application implements dagger.android.HasActivityInjector, dagger.android.HasFragmentInjector, androidx.lifecycle.LifecycleObserver {
    @org.jetbrains.annotations.Nullable()
    @kotlin.jvm.JvmField()
    @javax.inject.Inject()
    public dagger.android.DispatchingAndroidInjector<android.app.Activity> activityDispatchingAndroidInjector;
    @org.jetbrains.annotations.Nullable()
    @kotlin.jvm.JvmField()
    @javax.inject.Inject()
    public dagger.android.DispatchingAndroidInjector<android.app.Fragment> fragmentDispatchingAndroidInjector;
    private kotlinx.coroutines.Job job;
    
    public AppApplication() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public dagger.android.AndroidInjector<android.app.Activity> activityInjector() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public dagger.android.AndroidInjector<android.app.Fragment> fragmentInjector() {
        return null;
    }
    
    @java.lang.Override()
    public void onCreate() {
    }
}