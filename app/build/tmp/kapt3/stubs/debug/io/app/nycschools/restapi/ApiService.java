package io.app.nycschools.restapi;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\bf\u0018\u00002\u00020\u0001J\u0014\u0010\u0002\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00050\u00040\u0003H\'J\u0014\u0010\u0006\u001a\u000e\u0012\n\u0012\b\u0012\u0004\u0012\u00020\u00070\u00040\u0003H\'\u00a8\u0006\b"}, d2 = {"Lio/app/nycschools/restapi/ApiService;", "", "getSatDate", "Lio/reactivex/Observable;", "", "Lio/app/nycschools/Models/NycSatModel;", "getSchoolsList", "Lio/app/nycschools/Models/NycSchoolsModel;", "app_debug"})
public abstract interface ApiService {
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "s3k6-pzi2.json")
    public abstract io.reactivex.Observable<java.util.List<io.app.nycschools.Models.NycSchoolsModel>> getSchoolsList();
    
    @org.jetbrains.annotations.NotNull()
    @retrofit2.http.GET(value = "f9bf-2cp4.json")
    public abstract io.reactivex.Observable<java.util.List<io.app.nycschools.Models.NycSatModel>> getSatDate();
}