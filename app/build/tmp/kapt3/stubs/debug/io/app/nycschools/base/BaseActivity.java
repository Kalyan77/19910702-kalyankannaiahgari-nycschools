package io.app.nycschools.base;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0084\u0001\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\b\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0003\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\t\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0010\u000e\n\u0002\b\b\b\u0016\u0018\u00002\u00020\u00012\u00020\u00022\u00020\u00032\u00020\u0004B\u0005\u00a2\u0006\u0002\u0010\u0005J\u000e\u0010,\u001a\b\u0012\u0004\u0012\u00020\u000e0-H\u0016J\u0006\u0010.\u001a\u00020/J\u001e\u00100\u001a\u00020\u001d2\u0006\u00101\u001a\u0002022\u0006\u00103\u001a\u0002042\u0006\u00105\u001a\u00020\'J\u001c\u00106\u001a\u00020/2\b\u00107\u001a\u0004\u0018\u0001082\b\u00109\u001a\u0004\u0018\u00010:H\u0016J\b\u0010;\u001a\u00020/H\u0014J\b\u0010<\u001a\u00020/H\u0014J\b\u0010=\u001a\u00020/H\u0014J\u0010\u0010>\u001a\u00020/2\b\u0010?\u001a\u0004\u0018\u00010@J\u0006\u0010A\u001a\u00020/J\u0016\u0010B\u001a\u00020/2\u0006\u0010C\u001a\u00020@2\u0006\u0010D\u001a\u00020\'J\u0006\u0010E\u001a\u00020/J\u001a\u0010F\u001a\u00020/2\b\u00101\u001a\u0004\u0018\u0001022\b\u0010C\u001a\u0004\u0018\u00010@J\u000e\u0010G\u001a\b\u0012\u0004\u0012\u00020#0-H\u0016R\u001e\u0010\u0006\u001a\u00020\u00078\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\b\u0010\t\"\u0004\b\n\u0010\u000bR$\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u000e0\r8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u000f\u0010\u0010\"\u0004\b\u0011\u0010\u0012R\u001e\u0010\u0013\u001a\u00020\u00148\u0006@\u0006X\u0087.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R\u001a\u0010\u0019\u001a\u00020\u0007X\u0086.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001a\u0010\t\"\u0004\b\u001b\u0010\u000bR\u001c\u0010\u001c\u001a\u0004\u0018\u00010\u001dX\u0086\u000e\u00a2\u0006\u000e\n\u0000\u001a\u0004\b\u001e\u0010\u001f\"\u0004\b \u0010!R$\u0010\"\u001a\b\u0012\u0004\u0012\u00020#0\r8\u0000@\u0000X\u0081.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b$\u0010\u0010\"\u0004\b%\u0010\u0012R\u001e\u0010(\u001a\u00020\'2\u0006\u0010&\u001a\u00020\'@BX\u0086\u000e\u00a2\u0006\b\n\u0000\u001a\u0004\b(\u0010)R\u000e\u0010*\u001a\u00020+X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006H"}, d2 = {"Lio/app/nycschools/base/BaseActivity;", "Landroidx/appcompat/app/AppCompatActivity;", "Ldagger/android/HasActivityInjector;", "Ldagger/android/support/HasSupportFragmentInjector;", "Lio/app/nycschools/factory/Injectable;", "()V", "HomeViewModel", "Lio/app/nycschools/viewmodel/HomeViewModel;", "getHomeViewModel", "()Lio/app/nycschools/viewmodel/HomeViewModel;", "setHomeViewModel", "(Lio/app/nycschools/viewmodel/HomeViewModel;)V", "activityDispatchingAndroidInjector", "Ldagger/android/DispatchingAndroidInjector;", "Landroid/app/Activity;", "getActivityDispatchingAndroidInjector$app_debug", "()Ldagger/android/DispatchingAndroidInjector;", "setActivityDispatchingAndroidInjector$app_debug", "(Ldagger/android/DispatchingAndroidInjector;)V", "appFactory", "Landroidx/lifecycle/ViewModelProvider$Factory;", "getAppFactory", "()Landroidx/lifecycle/ViewModelProvider$Factory;", "setAppFactory", "(Landroidx/lifecycle/ViewModelProvider$Factory;)V", "baseHomeViewModle", "getBaseHomeViewModle", "setBaseHomeViewModle", "dialog", "Landroid/app/Dialog;", "getDialog", "()Landroid/app/Dialog;", "setDialog", "(Landroid/app/Dialog;)V", "fragmentDispatchingAndroidInjector", "Landroidx/fragment/app/Fragment;", "getFragmentDispatchingAndroidInjector$app_debug", "setFragmentDispatchingAndroidInjector$app_debug", "<set-?>", "", "isShown", "()Z", "mLastClickTime", "", "activityInjector", "Ldagger/android/AndroidInjector;", "closeProgress", "", "commonDialog", "context", "Landroid/content/Context;", "layoutDialog", "", "isCancelable", "onCreate", "savedInstanceState", "Landroid/os/Bundle;", "persistentState", "Landroid/os/PersistableBundle;", "onPause", "onResume", "onStart", "showAlertDialogForNoData", "s", "", "showAlertDialogForSplash", "showDialog", "message", "cancellable", "showProgress", "showToast", "supportFragmentInjector", "app_debug"})
public class BaseActivity extends androidx.appcompat.app.AppCompatActivity implements dagger.android.HasActivityInjector, dagger.android.support.HasSupportFragmentInjector, io.app.nycschools.factory.Injectable {
    public io.app.nycschools.viewmodel.HomeViewModel baseHomeViewModle;
    @javax.inject.Inject()
    public androidx.lifecycle.ViewModelProvider.Factory appFactory;
    @org.jetbrains.annotations.Nullable()
    private android.app.Dialog dialog;
    private boolean isShown = false;
    private long mLastClickTime = 0L;
    @javax.inject.Inject()
    public dagger.android.DispatchingAndroidInjector<android.app.Activity> activityDispatchingAndroidInjector;
    @javax.inject.Inject()
    public dagger.android.DispatchingAndroidInjector<androidx.fragment.app.Fragment> fragmentDispatchingAndroidInjector;
    @javax.inject.Inject()
    public io.app.nycschools.viewmodel.HomeViewModel HomeViewModel;
    private java.util.HashMap _$_findViewCache;
    
    public BaseActivity() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.app.nycschools.viewmodel.HomeViewModel getBaseHomeViewModle() {
        return null;
    }
    
    public final void setBaseHomeViewModle(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.viewmodel.HomeViewModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final androidx.lifecycle.ViewModelProvider.Factory getAppFactory() {
        return null;
    }
    
    public final void setAppFactory(@org.jetbrains.annotations.NotNull()
    androidx.lifecycle.ViewModelProvider.Factory p0) {
    }
    
    @java.lang.Override()
    public void onCreate(@org.jetbrains.annotations.Nullable()
    android.os.Bundle savedInstanceState, @org.jetbrains.annotations.Nullable()
    android.os.PersistableBundle persistentState) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final android.app.Dialog getDialog() {
        return null;
    }
    
    public final void setDialog(@org.jetbrains.annotations.Nullable()
    android.app.Dialog p0) {
    }
    
    public final boolean isShown() {
        return false;
    }
    
    @java.lang.Override()
    protected void onResume() {
    }
    
    @java.lang.Override()
    protected void onPause() {
    }
    
    public final void showToast(@org.jetbrains.annotations.Nullable()
    android.content.Context context, @org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    public final void showAlertDialogForSplash() {
    }
    
    public final void showProgress() {
    }
    
    public final void closeProgress() {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.app.Dialog commonDialog(@org.jetbrains.annotations.NotNull()
    android.content.Context context, int layoutDialog, boolean isCancelable) {
        return null;
    }
    
    public final void showAlertDialogForNoData(@org.jetbrains.annotations.Nullable()
    java.lang.String s) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dagger.android.DispatchingAndroidInjector<android.app.Activity> getActivityDispatchingAndroidInjector$app_debug() {
        return null;
    }
    
    public final void setActivityDispatchingAndroidInjector$app_debug(@org.jetbrains.annotations.NotNull()
    dagger.android.DispatchingAndroidInjector<android.app.Activity> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final dagger.android.DispatchingAndroidInjector<androidx.fragment.app.Fragment> getFragmentDispatchingAndroidInjector$app_debug() {
        return null;
    }
    
    public final void setFragmentDispatchingAndroidInjector$app_debug(@org.jetbrains.annotations.NotNull()
    dagger.android.DispatchingAndroidInjector<androidx.fragment.app.Fragment> p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public dagger.android.AndroidInjector<android.app.Activity> activityInjector() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final io.app.nycschools.viewmodel.HomeViewModel getHomeViewModel() {
        return null;
    }
    
    public final void setHomeViewModel(@org.jetbrains.annotations.NotNull()
    io.app.nycschools.viewmodel.HomeViewModel p0) {
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public dagger.android.AndroidInjector<androidx.fragment.app.Fragment> supportFragmentInjector() {
        return null;
    }
    
    @java.lang.Override()
    protected void onStart() {
    }
    
    public final void showDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, boolean cancellable) {
    }
}