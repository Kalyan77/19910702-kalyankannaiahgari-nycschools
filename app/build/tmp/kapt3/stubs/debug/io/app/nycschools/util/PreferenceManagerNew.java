package io.app.nycschools.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0010\u000e\n\u0002\b\f\n\u0002\u0018\u0002\n\u0002\b\t\n\u0002\u0010\u000b\n\u0002\b\u001b\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0006\n\u0002\u0010\u0002\n\u0002\b\t\n\u0002\u0018\u0002\n\u0002\b\u0005\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010?\u001a\u00020\u001cJ\b\u0010@\u001a\u00020AH\u0002J\u0006\u0010B\u001a\u00020AJ\u0006\u0010C\u001a\u00020AJ&\u0010D\u001a\u0002HE\"\u0006\b\u0000\u0010E\u0018\u00012\u0006\u0010F\u001a\u00020\u00052\u0006\u0010G\u001a\u0002HEH\u0082\b\u00a2\u0006\u0002\u0010HJ\u000e\u0010I\u001a\u00020A2\u0006\u0010J\u001a\u00020KJ\u000e\u0010L\u001a\u00020A2\u0006\u0010M\u001a\u00020\u001cJ(\u0010N\u001a\u00020A\"\u0006\b\u0000\u0010E\u0018\u00012\u0006\u0010F\u001a\u00020\u00052\b\u0010\u0013\u001a\u0004\u0018\u0001HEH\u0082\b\u00a2\u0006\u0002\u0010OR\u0014\u0010\u0003\u001a\b\u0012\u0004\u0012\u00020\u00050\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0006\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\b\u001a\u00020\u0005X\u0082D\u00a2\u0006\u0002\n\u0000R\u000e\u0010\t\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\n\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000b\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\f\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\r\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000e\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u000f\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0010\u001a\u00020\u0005X\u0082T\u00a2\u0006\u0002\n\u0000R\u001c\u0010\u0011\u001a\u0010\u0012\f\u0012\n\u0012\u0006\b\u0001\u0012\u00020\u00010\u00120\u0004X\u0082\u0004\u00a2\u0006\u0002\n\u0000R$\u0010\u0014\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00058F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u0015\u0010\u0016\"\u0004\b\u0017\u0010\u0018R$\u0010\u0019\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00058F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u001a\u0010\u0016\"\u0004\b\u001b\u0010\u0018R$\u0010\u001d\u001a\u00020\u001c2\u0006\u0010\u0013\u001a\u00020\u001c8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b\u001d\u0010\u001e\"\u0004\b\u001f\u0010 R$\u0010!\u001a\u00020\u001c2\u0006\u0010\u0013\u001a\u00020\u001c8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b!\u0010\u001e\"\u0004\b\"\u0010 R$\u0010#\u001a\u00020\u001c2\u0006\u0010\u0013\u001a\u00020\u001c8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b#\u0010\u001e\"\u0004\b$\u0010 R$\u0010%\u001a\u00020\u001c2\u0006\u0010\u0013\u001a\u00020\u001c8F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b&\u0010\u001e\"\u0004\b\'\u0010 R$\u0010(\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00058F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b)\u0010\u0016\"\u0004\b*\u0010\u0018R$\u0010+\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00058F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b,\u0010\u0016\"\u0004\b-\u0010\u0018R$\u0010.\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00058F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b/\u0010\u0016\"\u0004\b0\u0010\u0018R(\u00101\u001a\u0004\u0018\u00010\u00052\b\u0010\u0013\u001a\u0004\u0018\u00010\u00058F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b2\u0010\u0016\"\u0004\b3\u0010\u0018R$\u00104\u001a\u00020\u00052\u0006\u0010\u0013\u001a\u00020\u00058F@FX\u0086\u000e\u00a2\u0006\f\u001a\u0004\b5\u0010\u0016\"\u0004\b6\u0010\u0018R\u000e\u00107\u001a\u000208X\u0082.\u00a2\u0006\u0002\n\u0000R\u001a\u00109\u001a\u00020:X\u0080.\u00a2\u0006\u000e\n\u0000\u001a\u0004\b;\u0010<\"\u0004\b=\u0010>\u00a8\u0006P"}, d2 = {"Lio/app/nycschools/util/PreferenceManagerNew;", "", "()V", "CLEAR_PROFILE_PREFS_KEYS", "", "", "DEVICE_FCM_TOKEN", "EMP_ID", "FIRST_LAUNCH", "GroWero_PREFERENCES", "IS_FIRST_TIME_LOGIN", "IS_LOGIN", "LOGGED_AS", "LOGGED_NAME", "MOBILENUM", "PROFILE_MOBILE_NUMBER", "SECURE_PIN", "UNACCEPTABLE_PREFS_NULL_TYPES", "Lkotlin/reflect/KClass;", "value", "deviceToken", "getDeviceToken", "()Ljava/lang/String;", "setDeviceToken", "(Ljava/lang/String;)V", "employeeID", "getEmployeeID", "setEmployeeID", "", "isEmployeeLogged", "()Z", "setEmployeeLogged", "(Z)V", "isFirstTimeLogin", "setFirstTimeLogin", "isSalePersonLogged", "setSalePersonLogged", "islogin", "getIslogin", "setIslogin", "loggedInAs", "getLoggedInAs", "setLoggedInAs", "logged_name", "getLogged_name", "setLogged_name", "mobile", "getMobile", "setMobile", "profileMobileNumber", "getProfileMobileNumber", "setProfileMobileNumber", "securePin", "getSecurePin", "setSecurePin", "sharedPreferences", "Landroid/content/SharedPreferences;", "spEditor", "Landroid/content/SharedPreferences$Editor;", "getSpEditor$app_debug", "()Landroid/content/SharedPreferences$Editor;", "setSpEditor$app_debug", "(Landroid/content/SharedPreferences$Editor;)V", "FirstLaunch", "checkHasSharedPrefsInitialized", "", "clearAllPreferences", "clearProfilePreferences", "getFromPrefs", "T", "key", "default", "(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;", "init", "context", "Landroid/content/Context;", "setFirstTimeLaunch", "isFirstTime", "setToPrefs", "(Ljava/lang/String;Ljava/lang/Object;)V", "app_debug"})
public final class PreferenceManagerNew {
    @org.jetbrains.annotations.NotNull()
    public static final io.app.nycschools.util.PreferenceManagerNew INSTANCE = null;
    private static android.content.SharedPreferences sharedPreferences;
    private static final java.lang.String GroWero_PREFERENCES = "Nyc Prefs";
    private static final java.lang.String IS_FIRST_TIME_LOGIN = "firstTimeLogin";
    private static final java.lang.String PROFILE_MOBILE_NUMBER = "profileMobileNumber";
    private static final java.lang.String FIRST_LAUNCH = "firstLaunch";
    private static final java.lang.String SECURE_PIN = "securePin";
    private static final java.lang.String EMP_ID = "EMP_ID";
    private static final java.lang.String MOBILENUM = "mobile";
    private static final java.lang.String LOGGED_NAME = "LOGGED";
    private static final java.lang.String IS_LOGIN = "Islogin";
    private static final java.lang.String LOGGED_AS = "LOGGED_AS";
    private static final java.lang.String DEVICE_FCM_TOKEN = "DEVICE_FCM_TOKEN";
    public static android.content.SharedPreferences.Editor spEditor;
    private static final java.util.List<java.lang.String> CLEAR_PROFILE_PREFS_KEYS = null;
    private static final java.util.List<kotlin.reflect.KClass<? extends java.lang.Object>> UNACCEPTABLE_PREFS_NULL_TYPES = null;
    
    private PreferenceManagerNew() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final android.content.SharedPreferences.Editor getSpEditor$app_debug() {
        return null;
    }
    
    public final void setSpEditor$app_debug(@org.jetbrains.annotations.NotNull()
    android.content.SharedPreferences.Editor p0) {
    }
    
    public final void init(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public final void setFirstTimeLaunch(boolean isFirstTime) {
    }
    
    public final boolean FirstLaunch() {
        return false;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getMobile() {
        return null;
    }
    
    public final void setMobile(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getDeviceToken() {
        return null;
    }
    
    public final void setDeviceToken(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getEmployeeID() {
        return null;
    }
    
    public final void setEmployeeID(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLogged_name() {
        return null;
    }
    
    public final void setLogged_name(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getLoggedInAs() {
        return null;
    }
    
    public final void setLoggedInAs(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    public final boolean getIslogin() {
        return false;
    }
    
    public final void setIslogin(boolean value) {
    }
    
    public final boolean isEmployeeLogged() {
        return false;
    }
    
    public final void setEmployeeLogged(boolean value) {
    }
    
    public final boolean isSalePersonLogged() {
        return false;
    }
    
    public final void setSalePersonLogged(boolean value) {
    }
    
    public final boolean isFirstTimeLogin() {
        return false;
    }
    
    public final void setFirstTimeLogin(boolean value) {
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getProfileMobileNumber() {
        return null;
    }
    
    public final void setProfileMobileNumber(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getSecurePin() {
        return null;
    }
    
    public final void setSecurePin(@org.jetbrains.annotations.NotNull()
    java.lang.String value) {
    }
    
    public final void clearAllPreferences() {
    }
    
    public final void clearProfilePreferences() {
    }
    
    private final void checkHasSharedPrefsInitialized() {
    }
}