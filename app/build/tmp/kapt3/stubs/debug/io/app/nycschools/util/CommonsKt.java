package io.app.nycschools.util;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 2, d1 = {"\u0000X\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0003\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0003\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0005\n\u0002\u0018\u0002\n\u0002\b\u0002\u001a\u0016\u0010\u0000\u001a\u00020\u00012\u0006\u0010\u0002\u001a\u00020\u00012\u0006\u0010\u0003\u001a\u00020\u0001\u001a\u0016\u0010\u0004\u001a\u00020\u00052\u0006\u0010\u0006\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\b\u001a\u0014\u0010\t\u001a\u00020\n*\u00020\u00012\b\u0010\u000b\u001a\u0004\u0018\u00010\u0001\u001a\u0016\u0010\f\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u001a \u0010\f\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00012\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u001a\u0016\u0010\f\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u001a\u0016\u0010\u0010\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u001a \u0010\u0010\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00012\b\u0010\u0011\u001a\u0004\u0018\u00010\r\u001a\u0016\u0010\u0012\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u0001\u001a \u0010\u0012\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u0006\u001a\u0004\u0018\u00010\u00012\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u001a\u0016\u0010\u0012\u001a\u00020\u0005*\u0004\u0018\u00010\r2\b\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u001a\u0012\u0010\u0013\u001a\u00020\u0005*\u00020\u00142\u0006\u0010\u0015\u001a\u00020\u0016\u001a\u001c\u0010\u0017\u001a\u00020\u0005*\u0004\u0018\u00010\r2\u0006\u0010\u0018\u001a\u00020\u00012\u0006\u0010\u0007\u001a\u00020\b\u001a\n\u0010\u0019\u001a\u00020\u0005*\u00020\u0014\u001a\u001e\u0010\u001a\u001a\u00020\u0005*\u00020\u001b2\u0012\u0010\u001c\u001a\u000e\u0012\u0004\u0012\u00020\u001b\u0012\u0004\u0012\u00020\u00050\u001d\u001a\n\u0010\u001e\u001a\u00020\u0005*\u00020\u001f\u001a\f\u0010 \u001a\u00020\u0001*\u0004\u0018\u00010\r\u001a\f\u0010!\u001a\u00020\u0001*\u0004\u0018\u00010\r\u001a2\u0010\"\u001a\u00020\u0005*\u00020\u001f2\u0006\u0010\u0007\u001a\u00020\b2\u0006\u0010#\u001a\u00020\u00012\n\b\u0002\u0010$\u001a\u0004\u0018\u00010%2\n\b\u0002\u0010&\u001a\u0004\u0018\u00010\u0001\u00a8\u0006\'"}, d2 = {"checkDate", "", "date", "date1", "showAlertDialog", "", "message", "context", "Landroid/content/Context;", "isValidGSTNo", "", "str", "logError", "", "throwable", "", "logInfo", "data", "logWarning", "navigate", "Landroidx/fragment/app/Fragment;", "directions", "Landroidx/navigation/NavDirections;", "onToast", "string", "popBackStack", "setSingleClickListener", "Landroid/view/View;", "onSingleClick", "Lkotlin/Function1;", "showKeyboard", "Landroid/widget/EditText;", "tag", "toJson", "transformIntoDatePicker", "format", "maxDate", "Ljava/util/Date;", "from", "app_debug"})
public final class CommonsKt {
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String toJson(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$toJson) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String tag(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$tag) {
        return null;
    }
    
    public static final boolean isValidGSTNo(@org.jetbrains.annotations.NotNull()
    java.lang.String $this$isValidGSTNo, @org.jetbrains.annotations.Nullable()
    java.lang.String str) {
        return false;
    }
    
    public static final void logInfo(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logInfo, @org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    public static final void logInfo(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logInfo, @org.jetbrains.annotations.Nullable()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    java.lang.Object data) {
    }
    
    public static final void onToast(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$onToast, @org.jetbrains.annotations.NotNull()
    java.lang.String string, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
    
    public static final void logWarning(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logWarning, @org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    public static final void logWarning(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logWarning, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable throwable) {
    }
    
    public static final void logWarning(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logWarning, @org.jetbrains.annotations.Nullable()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable throwable) {
    }
    
    public static final void logError(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logError, @org.jetbrains.annotations.Nullable()
    java.lang.String message) {
    }
    
    public static final void logError(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logError, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable throwable) {
    }
    
    public static final void logError(@org.jetbrains.annotations.Nullable()
    java.lang.Object $this$logError, @org.jetbrains.annotations.Nullable()
    java.lang.String message, @org.jetbrains.annotations.Nullable()
    java.lang.Throwable throwable) {
    }
    
    public static final void navigate(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$navigate, @org.jetbrains.annotations.NotNull()
    androidx.navigation.NavDirections directions) {
    }
    
    public static final void popBackStack(@org.jetbrains.annotations.NotNull()
    androidx.fragment.app.Fragment $this$popBackStack) {
    }
    
    public static final void setSingleClickListener(@org.jetbrains.annotations.NotNull()
    android.view.View $this$setSingleClickListener, @org.jetbrains.annotations.NotNull()
    kotlin.jvm.functions.Function1<? super android.view.View, kotlin.Unit> onSingleClick) {
    }
    
    public static final void showKeyboard(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $this$showKeyboard) {
    }
    
    public static final void transformIntoDatePicker(@org.jetbrains.annotations.NotNull()
    android.widget.EditText $this$transformIntoDatePicker, @org.jetbrains.annotations.NotNull()
    android.content.Context context, @org.jetbrains.annotations.NotNull()
    java.lang.String format, @org.jetbrains.annotations.Nullable()
    java.util.Date maxDate, @org.jetbrains.annotations.Nullable()
    java.lang.String from) {
    }
    
    @org.jetbrains.annotations.NotNull()
    public static final java.lang.String checkDate(@org.jetbrains.annotations.NotNull()
    java.lang.String date, @org.jetbrains.annotations.NotNull()
    java.lang.String date1) {
        return null;
    }
    
    public static final void showAlertDialog(@org.jetbrains.annotations.NotNull()
    java.lang.String message, @org.jetbrains.annotations.NotNull()
    android.content.Context context) {
    }
}