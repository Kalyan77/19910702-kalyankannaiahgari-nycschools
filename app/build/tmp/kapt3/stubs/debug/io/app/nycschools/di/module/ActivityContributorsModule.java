package io.app.nycschools.di.module;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000*\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\b\'\u0018\u00002\u00020\u0001B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H\'J\b\u0010\u0005\u001a\u00020\u0006H\'J\b\u0010\u0007\u001a\u00020\bH\'J\b\u0010\t\u001a\u00020\nH\'J\b\u0010\u000b\u001a\u00020\fH\'\u00a8\u0006\r"}, d2 = {"Lio/app/nycschools/di/module/ActivityContributorsModule;", "", "()V", "baseActivity", "Lio/app/nycschools/base/BaseActivity;", "baseFragmentFragment", "Lio/app/nycschools/base/BaseFragment;", "nycSchoolsListActivity", "Lio/app/nycschools/activties/NycSchoolsListActivity;", "salesPersonsNycSchoolDataActivity", "Lio/app/nycschools/activties/NycSchoolDataActivity;", "splashActivity", "Lio/app/nycschools/activties/SplashActivity;", "app_debug"})
@dagger.Module()
public abstract class ActivityContributorsModule {
    
    public ActivityContributorsModule() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract io.app.nycschools.base.BaseActivity baseActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract io.app.nycschools.activties.SplashActivity splashActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract io.app.nycschools.activties.NycSchoolsListActivity nycSchoolsListActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract io.app.nycschools.activties.NycSchoolDataActivity salesPersonsNycSchoolDataActivity();
    
    @org.jetbrains.annotations.NotNull()
    @dagger.android.ContributesAndroidInjector()
    public abstract io.app.nycschools.base.BaseFragment baseFragmentFragment();
}